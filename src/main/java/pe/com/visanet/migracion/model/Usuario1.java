package pe.com.visanet.migracion.model;

/**
 *
 * @author Michael Galdamez
 */
public final class Usuario1 {
 
    private String Username;
    private String Grupo;
    private String Genero;
    private String IdInstitucion;
    private String TipoDocumento;
    private String NroDocumento;
    private String Nombre;
    private String Apellido;
    private String Email;
    private String Perfil;
    private String FamilyName;
    private String PreferredUsername;
    private String Name;
    private String GivenName;
 
    public Usuario1(String Username, String Grupo, String Genero, String IdInstitucion, String TipoDocumento,
                    String NroDocumento, String Nombre, String Apellido, String Email, String Perfil, String FamilyName,
                    String PreferredUsername, String Name, String GivenName) {
        setUsername(Username);
        setGrupo(Grupo);
        setGenero(Genero);
        setIdInstitucion(IdInstitucion);
        setTipoDocumento(TipoDocumento);
        setNroDocumento(NroDocumento);
        setNombre(Nombre);
        setApellido(Apellido);
        setEmail(Email);
        setPerfil(Perfil);
        setFamilyName(FamilyName);
        setPreferredUsername(PreferredUsername);
        setName(Name);
        setGivenName(GivenName);
    }

    public String getNroDocumento() {
        return NroDocumento;
    }

    public void setNroDocumento(String NroDocumento) {
        this.NroDocumento = NroDocumento;
    }
  
    public String getUsername() {
        if (Username == null)
        {
            Username = "";
        }
        return Username;
    }
 
    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getGrupo() {
        if (Grupo == null)
        {
            Grupo = "";
        }
        return Grupo;
    }
 
    public void setGrupo(String Grupo) {
        this.Grupo = Grupo;
    }
    
    public String getGenero() {
        if (Genero == null)
            Genero = "";
        return Genero;
    }
 
    public void setGenero(String Genero) {
        this.Genero = Genero;
    }
    
    public String getIdInstitucion() {
        if (IdInstitucion == null)
            IdInstitucion = "";
        return IdInstitucion;
    }
 
    public void setIdInstitucion(String IdInstitucion) {
        this.IdInstitucion = IdInstitucion;
    }
    
    public String getTipoDocumento() {
        if (TipoDocumento == null)
            TipoDocumento = "";
        return TipoDocumento;
    }
 
    public void setTipoDocumento(String TipoDocumento) {
        this.TipoDocumento = TipoDocumento;
    }
    
        public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPerfil() {
        return Perfil;
    }

    public void setPerfil(String Perfil) {
        this.Perfil = Perfil;
    }
    
    public String getFamilyName() {
        return FamilyName;
    }

    public void setFamilyName(String FamilyName) {
        this.FamilyName = FamilyName;
    }

    public String getPreferredUsername() {
        return PreferredUsername;
    }

    public void setPreferredUsername(String PreferredUsername) {
        this.PreferredUsername = PreferredUsername;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getGivenName() {
        return GivenName;
    }

    public void setGivenName(String GivenName) {
        this.GivenName = GivenName;
    }
}
