package pe.com.visanet.bomigracionpl;

import pe.com.visanet.migracion.model.ComercioGrupo;
import pe.com.visanet.migracion.model.Usuario;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.csvreader.CsvReader;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.logging.Level;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael Galdamez
 */
public class App {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    private static final String CSV_FILE_PATH1 = "C:/MigracionPE.csv";

    public static void main(String[] args)
    {
        String configFile = "config.json";
        JSONObject config = new JSONObject();
        if (args.length > 0)
        {
            configFile = args[0];
        }
        try
        {
            config = new JSONObject(new String(Files.readAllBytes(Paths.get(configFile))));
        }
        catch (JSONException ex)
        {
            System.out.println(configFile + " is not a valid JSON config file(" + ex.getMessage() + ").");
        }
        catch (IOException ex)
        {
            System.out.println(configFile + " can not be read(" + ex.getMessage() + ").");
        }
        d2csv d = new d2csv(config);
    }
    
    /*public static void main(String[] args) throws ParseException {
        String file = comercioTelepago;//args[0];
        //UpdateGroupUser(file);
        //String password = PasswordGenerator.getPassword(
	//	PasswordGenerator.MINUSCULAS+
	//	PasswordGenerator.MAYUSCULAS+
	//	PasswordGenerator.ESPECIALES,10);
        //System.out.println("Password: " + password );
        //UpdateUsuarios(file);
        //UpdateGroupUser2(file);
        //UpdateComercios(reporteMDD);
        //ConsultaUsuarios(CSV_FILE_PATH3);
        //Reporte();
        InsertarComercios(file);
        //InsertarUsuarios(file);
        //InsertarComerciosGrupo(file);
    }*/
    /*
    public static void CommerceCreate(String file) throws ParseException {
        try {

            DatabaseManager db = new DatabaseManager();
            List<Comercio> comercios = new ArrayList<>();

            CsvReader comercios_import = new CsvReader(file);
            comercios_import.readHeaders();

            while (comercios_import.readRecord()) {
                
                String ruc = comercios_import.get("RUC");
                String currency = comercios_import.get("Moneda");
                if(currency.equals("DOLARES")){
                    currency = "USD";
                }else if(currency.equals("SOLES")){
                    currency = "PEN";
                }
                String mcc = comercios_import.get("MCC");
                String email = comercios_import.get("Correo");
                String product_id = comercios_import.get("product_id");
                String commerce_code = comercios_import.get("Codigo_Comercio");
                String name = comercios_import.get("Nombre");
                String commerce_group = comercios_import.get("Codigo_Grupo");                
                String id = comercios_import.get("Id_Grupo");
                String nameGroup = comercios_import.get("Nombre_Grupo");
                String segment = comercios_import.get("Segment");                
                String tel = comercios_import.get("Telefono");                
                String tokenizer = comercios_import.get("Habilitar_Tokenizacion");
                String recurrent = comercios_import.get("Habilitar_Recurrencia");
                String show_names = comercios_import.get("Mostrar_FirstName_LastName");
                String show_quotas = comercios_import.get("Mostrar_Cuotas");
                String state = comercios_import.get("Estado_Comercio");
                String automated_settle = comercios_import.get("Liquidacion_Automatica");
                String link_type_id = comercios_import.get("Disenho_Link");
                if(link_type_id.equals("Link URL")){
                    link_type_id = "URL";
                }
                String commerce_logo = comercios_import.get("Logo_Comercio");                
                //String verified_by_visa = comercios_import.get("");
                String rule_engine = comercios_import.get("Rule_Engine");
                String sub_product = comercios_import.get("Sub-Producto");
                if(sub_product.equals("Pago Link")){
                    sub_product = "PLNK";
                }else if(sub_product.equals("Tu Vitrina")){
                    sub_product = "TVRN";
                }else if(sub_product.equals("Pago Web")){
                    sub_product = "PWB";
                }
                String mdd1 = comercios_import.get("MDD1");
                String mdd2 = comercios_import.get("MDD2");
                String mdd3 = comercios_import.get("MDD3");
                String mdd10 = comercios_import.get("MDD10");
                String mdd11 = comercios_import.get("MDD11");
                String mdd = "{\"es\":[{\"name\":\"MDD1\",\"value\":\"" + mdd1 +"\"},"
                                    + "{\"name\":\"MDD2\",\"value\":\"" + mdd2 +"\"},"
                                    + "{\"name\":\"MDD3\",\"value\":\"" + mdd3 +"\"},"
                                    + "{\"name\":\"MDD10\",\"value\":\"" + mdd10 +"\"},"
                                    + "{\"name\":\"MDD11\",\"value\":\"" + mdd11 +"\"}]}";
                //String special_fields = comercios_import.get("");
                String trx_flow = comercios_import.get("Flujo_Transaccional");
                if(trx_flow.equals("Antifraude + Verified by Visa + Autorizacion")){
                    trx_flow = "AF+VbV+AUT";
                }
                String pagolinkname = comercios_import.get("Nombre_Link_Pago");
                String is_cybersource_enabled = comercios_import.get("Cybersource");
                String verified_by_visa = comercios_import.get("Verified_By_Visa");                
                String subproduct_id = comercios_import.get("subproduct_id");                
                String user = "";//comercios_import.get("Nombre_Usuario");
                String password = "";//comercios_import.get("Password");
                String emailUser = "";//comercios_import.get("E-mail_Usuario");
                String stateGroup = "";//comercios_import.get("Estado_Grupo");
                String group_code = "";//comercios_import.get("Codigo_Grupo");

                comercios.add(new Comercio(commerce_group, ruc, name, currency, mcc, tel, email, tokenizer, recurrent,show_names, show_quotas, state, automated_settle, 
                        link_type_id, commerce_code, rule_engine, sub_product, mdd, trx_flow,id, nameGroup, user, emailUser, stateGroup, group_code, pagolinkname, 
                        is_cybersource_enabled, verified_by_visa, product_id, subproduct_id));

            }

            comercios_import.close();
            for (Comercio c : comercios) {

                long created = db.insertComercios(c.getCommerce_group(), c.getRuc(), c.getName(), c.getCurrency(), c.getMcc(), c.getTel(), c.getEmail(), c.getTokenizer(), 
                        c.getRecurrent(), c.getShow_names(), c.getShow_quotas(), c.getState(), c.getAutomated_settle(), c.getLink_type_id(), c.getCommerce_code(), 
                        c.getRule_engine(), c.getSub_product(), c.getMdd(),c.getTrx_flow(), c.getPagolinkname(), c.getCybersource(), c.getVerified_by_visa(), c.getProduct_id(),
                        c.getSubproduct_id());

                //if(i <= 0){
                //    long grupo = db.insertGroupComercios(c.getCommerce_group(), c.getName_comercioGrupo(), c.getUser(), c.getEmail_comercioGrupo(), c.getState_comercioGrupo(), c.getGroup_code());
                //    if (grupo <= 0) {
                //        System.out.println(c.getName_comercioGrupo() + " commerce_group not created");
                //    } else {
                //        System.out.println(c.getName_comercioGrupo() + " created");
                //        i++;
                //    }
                ///}
                //long plink = db.insertBOPLink(c.getCommerce_code(), c.getLink_type_id());
                        
                if (created <= 0) {
                    System.out.println(c.getName() + " commerce not created");
                } else {
                    System.out.println(c.getName() + " created");
                }
                //if (plink <= 0) {
                //    System.out.println("plink commerce code: " + c.getCommerce_code() + " not created");
                //} else {
                //    System.out.println("plink commerce code: " + c.getCommerce_code() + " created");
                //}
            }

        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }*/
    
    public static void InsertarComerciosGrupo(String file) throws ParseException {
        try {

            DatabaseManager db = new DatabaseManager();
            List<ComercioGrupo> comercioGrupo = new ArrayList<>();

            CsvReader comercios_import = new CsvReader(file);
            comercios_import.readHeaders();

            while (comercios_import.readRecord()) {
                String accessKeyId = comercios_import.get("accessKeyId");
                String name = comercios_import.get("name");
                String group_code = comercios_import.get("merchantGroupId");
                String state = comercios_import.get("isActive");
                if(state.equals("true")){
                    state = "Activo";
                }else if(state.equals("false")){
                    state = "Inactivo";
                }
                String user = comercios_import.get("userName");
                String email = comercios_import.get("email");
                
                comercioGrupo.add(new ComercioGrupo(accessKeyId, name, user, email, state, group_code));

            }

            comercios_import.close();
            for (ComercioGrupo c : comercioGrupo) {

                long grupo = db.insertGroupComercios(c.getAccessKeyId(), c.getName(), c.getUser(), c.getEmail(), c.getState(), c.getGroup_code());
                if (grupo <= 0) {
                    System.out.println(c.getName() + " commerce_group not created");
                } else {
                    System.out.println(c.getName() + " created");
                }
            }

        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }
    
    public static void InsertarUsuarios(String file) throws ParseException{
        try {
            DatabaseManager db = new DatabaseManager();
            CsvReader usuarios_import = new CsvReader(file);
            List<Usuario> usuarios = new ArrayList<>();

            usuarios_import.readHeaders();

            while (usuarios_import.readRecord()) {
                String nombre = usuarios_import.get("Nombre");
                String apellido = usuarios_import.get("Apellido");
                String perfil = usuarios_import.get("Perfil");
                String email = usuarios_import.get("Email");
                String comercio = usuarios_import.get("Codigo_Comercio");
                String ruc = usuarios_import.get("RUC");

                usuarios.add(new Usuario(nombre, apellido, perfil, email, comercio, ruc));
            }

            usuarios_import.close();

            for (Usuario us : usuarios) {
                
                String comer = us.getCodigoComercio();
                comer = comer.replace("'","");
                String nombreComercio = "";
                String customentity = "[ ";
                String[] tokens = comer.split(",");
                for(String t : tokens) {                  
                    nombreComercio = db.queryBOCommerce(t.trim());
                    if (nombreComercio.equals("")) {
                        System.out.println(t.trim() + " commerce not found");
                    }
                    customentity += "{\"id\":\"" + t + "\",\"name\":\"" + t + " - " + nombreComercio + "\"}, ";
                }
                customentity = customentity.substring(0, customentity.length() - 2);
                customentity += " ]";
                
                boolean created = CognitoManager.createCognitoAccount(us.getEmail(), us.getNombre(), us.getApellido(), customentity, us.getRuc());

                if (!created) {
                    System.out.println(us.getEmail().toLowerCase() + " Account not created");
                } else {
                    System.out.println(us.getEmail().toLowerCase() + " created");
                    //
                    if (!us.getPerfil().isEmpty()) {
                        String perfilReal = "";
                        if(us.getPerfil().equals("Administrador")){
                            perfilReal = "ComercioAdminTelepago"; //PerfilCome
                        }
                        CognitoManager.addUserToGroup(us.getEmail().toLowerCase(), perfilReal);
                    }
                }
            }

        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }
    
    public static void UpdateUsuarios(String file) throws ParseException{
        try {
            DatabaseManager db = new DatabaseManager();
            CsvReader usuarios_import = new CsvReader(file);
            List<Usuario> usuarios = new ArrayList<>();

            usuarios_import.readHeaders();

            while (usuarios_import.readRecord()) {
                String nombre = usuarios_import.get("NOMBRE");
                String apellido = usuarios_import.get("APELLIDO");
                String perfil = usuarios_import.get("PERFIL");
                String email = usuarios_import.get("EMAIL");
                String comercio = usuarios_import.get("CODIGO_COMERCIOS");
                String ruc = usuarios_import.get("RUC_COMERCIOS");
                
                usuarios.add(new Usuario(nombre, apellido, perfil, email, comercio, ruc));
            }

            usuarios_import.close();

            for (Usuario us : usuarios) {
                
                String comer = us.getCodigoComercio();
                comer = comer.replace("'","");
                String nombreComercio = "";
                String customentity = "[ ";
                String[] tokens = comer.split(",");
                for(String t : tokens) {                  
                    nombreComercio = db.queryBOCommerce(t.trim());
                    if (nombreComercio.equals("")) {
                        System.out.println(t.trim() + " commerce not found");
                    }
                    customentity += "{\"id\":\"" + t + "\",\"name\":\"" + t + " - " + nombreComercio + "\"}, ";
                }
                customentity = customentity.substring(0, customentity.length() - 2);
                customentity += " ]";
        
                boolean updated = CognitoManager.createCognitoAccountPassword(us.getEmail(), "", us.getNombre(), us.getApellido());

                if (!updated) {
                    System.out.println(us.getEmail() + " Account not updated");
                } else {
                    System.out.println(us.getEmail() + " updated");
                }
            }

        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }
    
    public static void ConsultaUsuarios(String file) throws ParseException{
        try {
            CsvReader usuarios_import = new CsvReader(file);
            List<Usuario> usuarios = new ArrayList<>();

            usuarios_import.readHeaders();

            while (usuarios_import.readRecord()) {
                String nombre = usuarios_import.get("NOMBRE");
                String apellido = usuarios_import.get("APELLIDO");
                String perfil = usuarios_import.get("PERFIL");
                String email = usuarios_import.get("EMAIL");
                String comercio = usuarios_import.get("CODIGO_COMERCIOS");
                String ruc = usuarios_import.get("RUC_COMERCIOS");

                usuarios.add(new Usuario(nombre, apellido, perfil, email, comercio, ruc));
            }

            usuarios_import.close();

            for (Usuario us : usuarios) {
                
                boolean result = CognitoManager.getUserReCreate(us.getEmail(), us.getPerfil());
                if (!result) {
                    System.out.println(us.getEmail() + " Process failed");
                } else {
                    System.out.println(us.getEmail() + " Process success");
                }
                
            }

        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }
    
    public static void Reporte(){
        List<String> updated = CognitoManager.getListUser("CONFIRMED");
         String fileName = "C:\\Users\\micha\\Downloads\\newCsvFile.csv";
          FileWriter writer = null;

        try {
            writer = new FileWriter(fileName);
            writer.append("sub");
            writer.append(',');
            writer.append("email_verified");
            writer.append(',');
            writer.append("name");
            writer.append(',');
            writer.append("given_name");
            writer.append(',');
            writer.append("family_name");
            writer.append(',');
            writer.append("custom:state");
            writer.append(',');
            writer.append("email");
            writer.append(',');
            writer.append("custom:entity");
            writer.append('\n');
            for (String us : updated) {              
                if(us.equals("N")){
                    writer.append('\n');
                }else{
                    writer.append(us);
                    writer.append(',');  
                }
            }
            System.out.println("CSV file is created...");
            try {
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void updateVerifyEmail(){
        boolean updated = CognitoManager.verifyEmail();

                if (!updated) {
                    System.out.println(" Account not updated");
                } else {
                    System.out.println(" Account updated");
                }
    }   
    
    public static void UpdateGroupUser(String file) throws ParseException{
        try {
            CsvReader usuarios_import = new CsvReader(file);
            List<Usuario> usuarios = new ArrayList<>();

            usuarios_import.readHeaders();

            while (usuarios_import.readRecord()) {
                String nombre = usuarios_import.get("NOMBRE");
                String apellido = usuarios_import.get("APELLIDO");
                String perfil = usuarios_import.get("PERFIL");
                String email = usuarios_import.get("Usuario");
                String comercio = usuarios_import.get("CODIGO_COMERCIOS");
                String ruc = usuarios_import.get("RUC_COMERCIOS");

                usuarios.add(new Usuario(nombre, apellido, perfil, email, comercio, ruc));
            }

            usuarios_import.close();
            String group = "ComercioAdmin"; //

            for (Usuario us : usuarios) {
        
                boolean updated = CognitoManager.addUserToGroup(us.getEmail(), group);

                if (!updated) {
                    System.out.println(us.getEmail() + " Account not updated");
                } else {
                    System.out.println(us.getEmail() + " updated");
                }
            }

        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }
    
    public static void UpdateGroupUser2(String file) throws ParseException{
        try {
            CsvReader usuarios_import = new CsvReader(file);

            usuarios_import.readHeaders();

            while (usuarios_import.readRecord()) {
                String email = usuarios_import.get("EMAIL");
                String antes = usuarios_import.get("GRUPOANTES");
                String despues = usuarios_import.get("GRUPODESPUES");
                
                boolean deleted = CognitoManager.removeUserToGroup(email, antes);
                if (!deleted) {
                    System.out.println(email + " Account not updated");
                } else {
                    System.out.println(email + " updated");
                }
                
                boolean updated = CognitoManager.addUserToGroup(email, despues);
                if (!updated) {
                    System.out.println(email + " Account not updated");
                } else {
                    System.out.println(email + " updated");
                }
            }
            usuarios_import.close();

        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }
    
    public static void UpdateComercios(String file) throws ParseException {
        try {

            DatabaseManager db = new DatabaseManager();

            CsvReader comercios_import = new CsvReader(file);
            comercios_import.readHeaders();

            while (comercios_import.readRecord()) {
                String commerce_code = comercios_import.get("Codigo_Comercio");
                String mdd1 = comercios_import.get("MDD1");
                String mdd2 = comercios_import.get("MDD2");
                String mdd3 = comercios_import.get("MDD3");
                String mdd10 = comercios_import.get("MDD10");
                String mdd11 = comercios_import.get("MDD11");
                String mdd = "{\"es\":[{\"name\":\"MDD1\",\"value\":\"" + mdd1 +"\"},"
                                    + "{\"name\":\"MDD2\",\"value\":\"" + mdd2 +"\"},"
                                    + "{\"name\":\"MDD3\",\"value\":\"" + mdd3 +"\"},"
                                    + "{\"name\":\"MDD10\",\"value\":\"" + mdd10 +"\"},"
                                    + "{\"name\":\"MDD11\",\"value\":\"" + mdd11 +"\"}]}";
                
               long updated = db.updateComercios(commerce_code, mdd);
                if (updated <= 0) {
                    System.out.println(commerce_code + " commerce not updated");
                } else {
                    System.out.println(commerce_code + " updated");
                }           
            }
            comercios_import.close();
        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }
}