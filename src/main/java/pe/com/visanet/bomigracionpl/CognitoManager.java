package pe.com.visanet.bomigracionpl;

import pe.com.visanet.migracion.model.CognitoAccount;
import pe.com.visanet.migracion.model.Usuario1;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidp.model.AdminRemoveUserFromGroupRequest;
import com.amazonaws.services.cognitoidp.model.AddCustomAttributesRequest;
import com.amazonaws.services.cognitoidp.model.AdminAddUserToGroupRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminDeleteUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminDisableUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminEnableUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminGetUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminGetUserResult;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AdminListGroupsForUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminListGroupsForUserResult;
import com.amazonaws.services.cognitoidp.model.AdminResetUserPasswordRequest;
import com.amazonaws.services.cognitoidp.model.AdminRespondToAuthChallengeRequest;
import com.amazonaws.services.cognitoidp.model.AdminRespondToAuthChallengeResult;
import com.amazonaws.services.cognitoidp.model.AdminUpdateUserAttributesRequest;
import com.amazonaws.services.cognitoidp.model.AdminUpdateUserAttributesResult;
import com.amazonaws.services.cognitoidp.model.AttributeDataType;
import com.amazonaws.services.cognitoidp.model.AttributeType;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import com.amazonaws.services.cognitoidp.model.ChallengeNameType;
import com.amazonaws.services.cognitoidp.model.DeliveryMediumType;
import com.amazonaws.services.cognitoidp.model.GetUserRequest;
import com.amazonaws.services.cognitoidp.model.GetUserResult;
import com.amazonaws.services.cognitoidp.model.GroupType;
import com.amazonaws.services.cognitoidp.model.InvalidParameterException;
import com.amazonaws.services.cognitoidp.model.ListUsersRequest;
import com.amazonaws.services.cognitoidp.model.ListUsersResult;
import com.amazonaws.services.cognitoidp.model.SchemaAttributeType;
import com.amazonaws.services.cognitoidp.model.StringAttributeConstraintsType;
import com.amazonaws.services.cognitoidp.model.UserType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.sf.kdgcommons.lang.StringUtil;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAccount;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael
 */
public class CognitoManager extends AuthorizingRealm {

    private static final Logger LOGGER = LoggerFactory.getLogger(CognitoManager.class);
    protected static final String USERPOOLID = System.getProperty("COGNITO_USER_POOL_ID", "us-west-2_vSrvNhknk"); //us-east-1_pQHHu111b us-east-1_8LntJGJFC us-east-2_LfCbduwsG    
    protected static final String CLIENTID = System.getProperty("COGNITO_CLIENT_ID", "ua1aar0st24rr5hbtes0jns0i"); //42l9rttp5eruet5gpjbc4ub481 3pd3j8egkm3m9m4kifpv9v3i4n j0kpn35m0jr1gkkjef7vkl62c 5i7amsbclgija36tv17oufm0sh     
    protected static final String TEMPORARYPASSWORD = "xl9Stg19#"; //xl9Stg19#
    protected static final BasicAWSCredentials credentials = new BasicAWSCredentials("AKIA5EKBNDOUPN6OMXPF", "WiVh/QpycGj3aq3UWzg7PrshAYyKiPmGDjIbIqe0");//AKIAJLHKCP4INCUJ67EA oSpR/hEWyA0Xi0GPctk3pJ+JH/76DTiL4aQv+GmJ PROD (  )  AKIAJQRHXJAJS45MC5EQ  OGLlYRHSQq8CJjItngBYOZ5IMMPmXa4TUzMcjFJ4
    // AKIAJPLXPO2AGMPZD5DA wwhREopQswznJzd1VPtS/8W1solrQ6BTnEmDxNXa
    protected static AWSCognitoIdentityProviderClient cognitoClient = new AWSCognitoIdentityProviderClient(credentials);

    public static GetUserResult getUser(String accessToken) {
        GetUserRequest authRequest = new GetUserRequest().withAccessToken(accessToken);
        return cognitoClient.getUser(authRequest);
    }
    
    public static List<String> getListUser(String status) {
        List<String> users = new ArrayList<>();
        try {
            //cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_2));
            ListUsersRequest listUsersRequest = new ListUsersRequest()
                .withUserPoolId(USERPOOLID)
                .withLimit(10)
                //.withAttributesToGet("email", "cognito:user_status", "name", "family_name", "custom:state", "custom:entity")
                .withFilter("cognito:user_status=\""+status+"\"");           

            ListUsersResult listUsersResult = cognitoClient.listUsers(listUsersRequest);
            if (listUsersResult != null) {
                for (UserType u : listUsersResult.getUsers()) {
                    List<AttributeType> attributeList = u.getAttributes();
                    for (AttributeType attribute : attributeList) {
                        users.add(attribute.getValue());
                    }
                    users.add("N");
                }
            }
            String token = listUsersResult.getPaginationToken();
            while (token!=null) {
                listUsersRequest.withPaginationToken(token);
                listUsersResult = cognitoClient.listUsers(listUsersRequest);
                if (listUsersResult != null) {
                    for (UserType u : listUsersResult.getUsers()) {
                        List<AttributeType> attributeList = u.getAttributes();
                        for (AttributeType attribute : attributeList) {
                            users.add(attribute.getValue());
                        }
                        users.add("N");
                    }
                }
                token = listUsersResult.getPaginationToken();
            }
        } catch (InvalidParameterException ex) {
            System.out.println(ex.getLocalizedMessage());
            return null;
        }
        return users;
    }
    
    public static boolean getUserReCreate(String username, String perfil){
        boolean result = false;
        if (username == null) {
            return result;
        } else if (username.isEmpty()) {
            return result;
        }
        String nombre = "";
        String apellido = "";
        String customentity = "";
        String ruc = "";
        try {
            //cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_2));
            AdminGetUserRequest adminGetUserRequest = new AdminGetUserRequest()
                .withUserPoolId(USERPOOLID)
                .withUsername(username);
            AdminGetUserResult adminGetUserResult = cognitoClient.adminGetUser(adminGetUserRequest);
        //FORCE_CHANGE_PASSWORD
            if(adminGetUserResult.getUserStatus().equals("CONFIRMED")){
                List<AttributeType> attributes = adminGetUserResult.getUserAttributes();

                if (attributes != null) {
                    for (AttributeType u : attributes) {
                        switch (u.getName()) {
                            case "name":
                                nombre = u.getValue();
                                break;
                            case "given_name":
                                ruc = u.getValue();
                                break;
                            case "family_name":
                                apellido = u.getValue();
                                break;
                            case "custom:entity":
                                customentity = u.getValue();
                                break;
                        }
                    }
                }
                updateStatusAccount(username, false);
                AdminDeleteUserRequest adminDeleteUserRequest = new AdminDeleteUserRequest()
                            .withUserPoolId(USERPOOLID)
                            .withUsername(username);
                cognitoClient.adminDeleteUser(adminDeleteUserRequest);
                result = true;
            }
            if (!result) {
                System.out.println(username + " Account not deleted");
            } else {
                System.out.println(username + " deleted");
                boolean created = createCognitoAccount(username, nombre, apellido, customentity, ruc);
                if (!created) {
                    System.out.println(username + " Account not created");
                } else {
                    System.out.println(username + " created");
                    //
                    if (!perfil.isEmpty()) {
                        String perfilReal = "";
                        if(perfil.equals("Administrador")){
                            perfilReal = "ComercioAdmin"; //PerfilCome
                        }
                        CognitoManager.addUserToGroup(username.toLowerCase(), perfilReal);
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            result = false;
        }
        return result;
    }
    
    public static boolean verifyEmail(){
        boolean result = false;
        String email_verified = "";
        String email_user = "";
        List<String> users = new ArrayList<>();
        try {
            //cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_2));
            ListUsersRequest listUsersRequest = new ListUsersRequest()
                .withUserPoolId(USERPOOLID)
                .withLimit(10);
                //.withAttributesToGet("email", "cognito:user_status", "name", "family_name", "custom:state", "custom:entity")
                //.withFilter("email_verified=\""+status+"\"");           

            ListUsersResult listUsersResult = cognitoClient.listUsers(listUsersRequest);
            if (listUsersResult != null) {
                for (UserType u : listUsersResult.getUsers()) {
                    email_user = u.getUsername();
                    /*List<AttributeType> attributeList = u.getAttributes();
                    for (AttributeType attribute : attributeList) {
                        switch (attribute.getName()) {
                            case "email_verified":
                                email_verified = attribute.getValue();
                                break;
                            }
                        //users.add(attribute.getValue());
                    }*/
                    AdminUpdateUserAttributesRequest cognitoRequest = new AdminUpdateUserAttributesRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email_user)
                    .withUserAttributes(
                        new AttributeType()
                                .withName("email_verified")
                                .withValue("true")
                    );
                    try{
                        AdminUpdateUserAttributesResult x = cognitoClient.adminUpdateUserAttributes(cognitoRequest);
                        System.out.println("Actualizado: " + email_user);
                    } catch (Exception ex) {
                        System.out.println(ex.getLocalizedMessage());
                    }
                    
                    //users.add("N");
                }
            }
            String token = listUsersResult.getPaginationToken();
            while (token!=null) {
                listUsersRequest.withPaginationToken(token);
                listUsersResult = cognitoClient.listUsers(listUsersRequest);
                if (listUsersResult != null) {
                    for (UserType u : listUsersResult.getUsers()) {
                        email_user = u.getUsername();
                        AdminUpdateUserAttributesRequest cognitoRequest = new AdminUpdateUserAttributesRequest()
                        .withUserPoolId(USERPOOLID)
                        .withUsername(email_user)
                        .withUserAttributes(
                            new AttributeType()
                                    .withName("email_verified")
                                    .withValue("true")
                        );

                        try{
                            AdminUpdateUserAttributesResult x = cognitoClient.adminUpdateUserAttributes(cognitoRequest);
                            System.out.println("Actualizado: " + email_user);
                        } catch (Exception ex) {
                            System.out.println(ex.getLocalizedMessage());
                        }
                        //users.add("N");
                    }
                }
                token = listUsersResult.getPaginationToken();
            }
        } catch (InvalidParameterException ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        return result;
    }
    
    public static List<String> getListGroupUser(String username) {
        AdminListGroupsForUserRequest groupRequest = new AdminListGroupsForUserRequest()
                .withUsername(username)
                .withUserPoolId(USERPOOLID);
        try {
            AdminListGroupsForUserResult groupResult = cognitoClient.adminListGroupsForUser(groupRequest);
            List<String> roles = new ArrayList<>();
            if (groupResult != null) {
                for (GroupType g : groupResult.getGroups()) {
                    roles.add(g.getGroupName());
                }
            }
            return roles;
        } catch (Exception e) {
            List<String> roles = new ArrayList<>();
            return roles;
        }
    }

    public static AdminInitiateAuthResult refreshToken(String refreshToken) {
        Map<String, String> authParams = new HashMap<>();
        authParams.put("REFRESH_TOKEN", refreshToken);

        AdminInitiateAuthRequest refreshRequest = new AdminInitiateAuthRequest()
                .withAuthFlow(AuthFlowType.REFRESH_TOKEN)
                .withAuthParameters(authParams)
                .withClientId(CLIENTID)
                .withUserPoolId(USERPOOLID);

        return cognitoClient.adminInitiateAuth(refreshRequest);
    }

    public static CognitoAccount authenticateAccount(String userName, String password) {
        CognitoAccount account = null;
        try {
            AuthenticationToken token = new UsernamePasswordToken(userName, password);
            CognitoAuthenticationInfo auth = authenticate(token);
            SimpleAccount simpleAccount = (SimpleAccount) auth.getAuthentication();

            if (simpleAccount != null) {
                List<String> roles = getRoles(userName);
                account = new CognitoAccount();
                account.setEmail(userName);
                account.setGroups(roles);
                account.setName(userName);
                account.setAccessToken(auth.getAccessToken());

                getUserAttributes(userName, auth.getAccessToken());
            }

        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        }

        return account;
    }

    public static boolean addUserToGroup(String email, String group) {
        cognitoClient.setRegion(Region.getRegion(Regions.US_WEST_2)); //US_EAST_1
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }

        try {
            AdminAddUserToGroupRequest cognitoRequest = new AdminAddUserToGroupRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email)
                    .withGroupName(group);

            cognitoClient.adminAddUserToGroup(cognitoRequest);
            result = true;
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = false;
        }
        return result;
    }
    
    public static boolean removeUserToGroup(String email, String group) {
        //cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_2));
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }

        try {
            AdminRemoveUserFromGroupRequest cognitoRequest = new AdminRemoveUserFromGroupRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email)
                    .withGroupName(group);

            cognitoClient.adminRemoveUserFromGroup(cognitoRequest);
            result = true;
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = false;
        }

        return result;
    }

    public static boolean createCognitoAccount(String email, String firstName, String lastName, String customentity, String ruc) {
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }

        try {
            //cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_2));
            AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email.toLowerCase())
                    .withUserAttributes(
                            new AttributeType()
                                    .withName("email")
                                    .withValue(email.toLowerCase()),
                            new AttributeType()
                                    .withName("email_verified")
                                    .withValue("true"),
                            new AttributeType()
                                    .withName("name")
                                    .withValue(firstName),
                            new AttributeType()
                                    .withName("family_name")
                                    .withValue(lastName),
                            new AttributeType()
                                    .withName("custom:state")
                                    .withValue("Activo"),
                            new AttributeType()
                                    .withName("given_name")
                                    .withValue(ruc),
                            new AttributeType()
                                    .withName("custom:entity")
                                    .withValue(customentity)
                    )
                    .withDesiredDeliveryMediums(DeliveryMediumType.EMAIL)
                    .withForceAliasCreation(Boolean.FALSE);
                    
            cognitoClient.adminCreateUser(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            result = false;
        }
        return result;
    }
    
    public static boolean createCognitoAccountNew(Usuario1 user) {
        boolean result = false;
        if (user.getEmail() == null) {
            return result;
        } else if (user.getEmail().isEmpty()) {
            return result;
        }

        try {
            cognitoClient.setRegion(Region.getRegion(Regions.US_WEST_2));
            AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(user.getUsername())
                    .withUserAttributes(
                            new AttributeType()
                                    .withName("email")
                                    .withValue(user.getEmail().toLowerCase()),
                            new AttributeType()
                                    .withName("email_verified")
                                    .withValue("true"),
                            new AttributeType()
                                    .withName("custom:genero")
                                    .withValue(user.getGenero()),
                            new AttributeType()
                                    .withName("custom:idinstitucion")
                                    .withValue(user.getIdInstitucion()),
                            new AttributeType()
                                    .withName("custom:tipodocumento")
                                    .withValue(user.getTipoDocumento()),
                            new AttributeType()
                                    .withName("custom:nrodocumento")
                                    .withValue(user.getNroDocumento()),
                            new AttributeType()
                                    .withName("custom:nombres")
                                    .withValue(user.getNombre()),
                            new AttributeType()
                                    .withName("custom:apellidos")
                                    .withValue(user.getApellido()),
                            new AttributeType()
                                    .withName("custom:idperfil")
                                    .withValue(user.getPerfil()),
                            new AttributeType()
                                    .withName("preferred_username")
                                    .withValue(user.getPreferredUsername()),
                            new AttributeType()
                                    .withName("given_name")
                                    .withValue(user.getGivenName()),
                            new AttributeType()
                                    .withName("name")
                                    .withValue(user.getName()),
                            new AttributeType()
                                    .withName("family_name")
                                    .withValue(user.getFamilyName())
                    )
                    .withDesiredDeliveryMediums(DeliveryMediumType.EMAIL)
                    .withForceAliasCreation(Boolean.FALSE);
                    
            cognitoClient.adminCreateUser(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            result = false;
        }
        return result;
    }
    
    public static boolean deleteCognitoAccount(String username, String perfil){
        boolean result = false;
        if (username == null) {
            return result;
        } else if (username.isEmpty()) {
            return result;
        }

        try {
            cognitoClient.setRegion(Region.getRegion(Regions.US_WEST_2));
            AdminGetUserRequest adminGetUserRequest = new AdminGetUserRequest()
                .withUserPoolId(USERPOOLID)
                .withUsername(username);
            AdminGetUserResult adminGetUserResult = cognitoClient.adminGetUser(adminGetUserRequest);
            //FORCE_CHANGE_PASSWORD
            //if(adminGetUserResult.getUserStatus().equals("FORCE_CHANGE_PASSWORD")){
                AdminDeleteUserRequest adminDeleteUserRequest = new AdminDeleteUserRequest()
                            .withUserPoolId(USERPOOLID)
                            .withUsername(username);
                cognitoClient.adminDeleteUser(adminDeleteUserRequest);
                result = true;
            //}
            if (!result) {
                System.out.println(username + " Account not deleted");
            } else {
                System.out.println(username + " deleted");
            }
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            result = false;
        }
        return result;
    }

    public static boolean createCognitoAccountPassword(String email, String password, String firstName, String lastName) {
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }

        try {
            password = "?9u-x$$E";
            Map<String,String> initialParams = new HashMap<>();
            initialParams.put("USERNAME", email);
            initialParams.put("PASSWORD", TEMPORARYPASSWORD);
            //cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_2));
            String pswd = PasswordGenerator.getPassword( PasswordGenerator.MINUSCULAS+PasswordGenerator.MAYUSCULAS+ PasswordGenerator.ESPECIALES,10);
            AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email)
                    .withUserAttributes(
                            new AttributeType()
                                    .withName("email")
                                    .withValue(email),
                            new AttributeType()
                                    .withName("email_verified")
                                    .withValue("true"),
                            new AttributeType()
                                    .withName("name")
                                    .withValue(firstName),
                            new AttributeType()
                                    .withName("family_name")
                                    .withValue(lastName)
                            /*new AttributeType()
                                    .withName("profile")
                                    .withValue(serviceId),
                            new AttributeType()
                                    .withName("zoneinfo")
                                    .withValue(ruc)*/
                    )
                    .withTemporaryPassword(pswd)
                    .withForceAliasCreation(Boolean.FALSE);
            
            cognitoClient.adminCreateUser(cognitoRequest);

            //.withDesiredDeliveryMediums(DeliveryMediumType.EMAIL)
            /*AdminConfirmSignUpRequest signupRequest = new AdminConfirmSignUpRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email);

            cognitoClient.adminConfirmSignUp(signupRequest);*/
            
            AdminInitiateAuthRequest initialRequest = new AdminInitiateAuthRequest()
                    .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                    .withAuthParameters(initialParams)
                    .withClientId(CLIENTID)
                    .withUserPoolId(USERPOOLID);

            AdminInitiateAuthResult initialResponse = cognitoClient.adminInitiateAuth(initialRequest);
            if (!ChallengeNameType.NEW_PASSWORD_REQUIRED.name().equals(initialResponse.getChallengeName()))
            {
                throw new RuntimeException("unexpected challenge: " + initialResponse.getChallengeName());
            }

            Map<String,String> challengeResponses = new HashMap<>();
            challengeResponses.put("USERNAME", email);
            challengeResponses.put("PASSWORD", TEMPORARYPASSWORD);
            challengeResponses.put("NEW_PASSWORD", password);

            AdminRespondToAuthChallengeRequest finalRequest = new AdminRespondToAuthChallengeRequest()
                    .withChallengeName(ChallengeNameType.NEW_PASSWORD_REQUIRED)
                    .withChallengeResponses(challengeResponses)
                    .withClientId(CLIENTID)
                    .withUserPoolId(USERPOOLID)
                    .withSession(initialResponse.getSession());

            AdminRespondToAuthChallengeResult challengeResponse = cognitoClient.adminRespondToAuthChallenge(finalRequest);
            
            updateStatusAccount(email, true);
            result = StringUtil.isBlank(challengeResponse.getChallengeName());
            
            if (!result) {
                throw new RuntimeException("unexpected challenge: " + challengeResponse.getChallengeName());
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
            result = false;
        }
        return result;
    }

    public static void AddCustomAttributes(String comercio) {
        AddCustomAttributesRequest customAttributtesRequest = new AddCustomAttributesRequest()
                .withUserPoolId(USERPOOLID)
                .withCustomAttributes(
                    new SchemaAttributeType()
                        .withAttributeDataType(AttributeDataType.String)
                        .withName(comercio)
                        .withMutable(Boolean.TRUE)
                        .withStringAttributeConstraints(
                            new StringAttributeConstraintsType()
                                .withMinLength("1")
                                .withMaxLength("20")
                        )
                );
        cognitoClient.addCustomAttributes(customAttributtesRequest);
    }
    
    public static void updateStatusAccount(String email, boolean status) {
        if (status) {
            AdminEnableUserRequest enableRequest = new AdminEnableUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email);

            cognitoClient.adminEnableUser(enableRequest);
        } else {
            AdminDisableUserRequest disableRequest = new AdminDisableUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email);

            cognitoClient.adminDisableUser(disableRequest);
        }
    }

    public static boolean updateCognitoAccount(String email, String firstName, String lastName, String customentity, String ruc) {
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }
        
        try {
            cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_2));
            AdminUpdateUserAttributesRequest cognitoRequest = new AdminUpdateUserAttributesRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email.toLowerCase())
                    .withUserAttributes(
                            new AttributeType()
                                    .withName("email")
                                    .withValue(email),
                            new AttributeType()
                                    .withName("email_verified")
                                    .withValue("true")
                            /*new AttributeType()
                                    .withName("name")
                                    .withValue(firstName),
                            new AttributeType()
                                    .withName("family_name")
                                    .withValue(lastName),
                            new AttributeType()
                                    .withName("given_name")
                                    .withValue(ruc),
                            new AttributeType()
                                    .withName("custom:entity")
                                    .withValue(customentity)*/
                    );

            cognitoClient.adminUpdateUserAttributes(cognitoRequest);

            //updateStatusAccount(email, true);

            result = true;
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
            result = false;
        }

        return result;
    }

    public static boolean resetCognitoPassword(String email) {
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }

        try {
            AdminResetUserPasswordRequest cognitoRequest = new AdminResetUserPasswordRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email);

            cognitoClient.adminResetUserPassword(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            result = false;
        }

        return result;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        AdminListGroupsForUserRequest groupRequest = new AdminListGroupsForUserRequest()
                .withUsername(username)
                .withUserPoolId(USERPOOLID);

        AdminListGroupsForUserResult groupResult = cognitoClient.adminListGroupsForUser(groupRequest);
        Set<String> roles = new HashSet<>();
        if (groupResult != null) {
            for (GroupType g : groupResult.getGroups()) {
                roles.add(g.getGroupName());
            }
        }

        info.addRoles(roles);
        return info;
    }

    private static List<String> getRoles(String username) {
        AdminListGroupsForUserRequest groupRequest = new AdminListGroupsForUserRequest()
                .withUsername(username)
                .withUserPoolId(USERPOOLID);

        AdminListGroupsForUserResult groupResult = cognitoClient.adminListGroupsForUser(groupRequest);
        List<String> roles = new ArrayList<>();
        if (groupResult != null) {
            for (GroupType g : groupResult.getGroups()) {
                roles.add(g.getGroupName());
            }
        }

        return roles;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        return authenticate(token).getAuthentication();
    }

    private static CognitoAuthenticationInfo authenticate(AuthenticationToken token) throws AuthenticationException {
        final String password = new String((char[]) token.getCredentials());
        final String username = (String) token.getPrincipal();

        Map<String, String> authParams = new HashMap<>();
        authParams.put("USERNAME", username);
        authParams.put("PASSWORD", password);

        AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest()
                .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                .withAuthParameters(authParams)
                .withClientId(CLIENTID)
                .withUserPoolId(USERPOOLID);

        AdminInitiateAuthResult authResponse = cognitoClient.adminInitiateAuth(authRequest);

        if (authResponse.getChallengeName() == null) {
            PrincipalCollection principals = null;
            String authtoken = "";
            try {
                authtoken = authResponse.getAuthenticationResult().getAccessToken();
                principals = createPrincipals(username, authtoken);
            } catch (Exception e) {
                throw new AuthenticationException("Unable to obtain authenticated account properties.", e);
            }
            SimpleAccount account = new SimpleAccount(principals, token.getCredentials());

            return new CognitoAuthenticationInfo(account, authtoken);
        }

        return null;
    }

    @Override
    protected Object getAuthenticationCacheKey(PrincipalCollection principals) {
        if (!CollectionUtils.isEmpty(principals)) {
            Collection thisPrincipals = principals.fromRealm(getName());
            if (CollectionUtils.isEmpty(thisPrincipals)) {
                //no principals attributed to this particular realm.  Fall back to the 'master' primary:
                return principals.getPrimaryPrincipal();

            } else {
                Iterator iterator = thisPrincipals.iterator();
                iterator.next(); //First item is the Stormpath' account href
                //Second item is Stormpath' account map
                Map<String, Object> accountInfo = (Map<String, Object>) iterator.next();
                //Users can indistinctively login using their emails or usernames. Therefore, we need to try which is
                //the key used in each case
                String email = (String) accountInfo.get("email");
                if (getAuthenticationCache().get(email) != null) {
                    return email;
                }
                return accountInfo.get("username");
            }
        }
        return null;
    }

    private static void nullSafePut(Map<String, String> props, String propName, String value) {
        String cleanValue = StringUtils.clean(value);
        if (cleanValue != null) {
            props.put(propName, cleanValue);
        }
    }

    protected static PrincipalCollection createPrincipals(String username, String accessToken) {

        Collection<Object> principals = new ArrayList<>(2);
        principals.add(username);
        principals.add(getUserAttributes(username, accessToken));

        return new SimplePrincipalCollection(principals, CognitoManager.class.getName());
    }

    private static Map<String, String> getUserAttributes(String username, String accesstoken) {
        Map<String, String> props = new LinkedHashMap<>();

        nullSafePut(props, "username", username);
        nullSafePut(props, "email", username);

        GetUserRequest userRequest = new GetUserRequest().withAccessToken(accesstoken);
        GetUserResult userResponse = cognitoClient.getUser(userRequest);

        for (AttributeType att : userResponse.getUserAttributes()) {
            nullSafePut(props, att.getName(), att.getValue());
        }

        return props;
    }

}
