package pe.com.visanet.migracion.model;

import javax.validation.constraints.Size;

/**
 *
 * @author Michael Galdamez
 */
public final class Usuario {
 
    private String nombre;
    private String apellido;
    private String perfil;
    private String email;
    private String codigoComercio;
    @Size(max = 11)
    private String ruc;
 
    public Usuario(String nombre, String apellido, String perfil, String email, String codigoComercio, String ruc) {
        setNombre(nombre);
        setApellido(apellido);
        setPerfil(perfil);
        setEmail(email);
        setCodigoComercio(codigoComercio);
        setRuc(ruc);
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
  
    public String getNombre() {
        if (nombre == null)
        nombre = "";
        return nombre;
    }
 
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        if (apellido == null)
        apellido = "";
        return apellido;
    }
 
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public String getPerfil() {
        if (perfil == null)
        perfil = "";
        return perfil;
    }
 
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
    
    public String getEmail() {
        if (email == null)
        email = "";
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getCodigoComercio() {
        if (codigoComercio == null)
        codigoComercio = "";
        return codigoComercio;
    }
 
    public void setCodigoComercio(String codigoComercio) {
        this.codigoComercio = codigoComercio;
    }
}
