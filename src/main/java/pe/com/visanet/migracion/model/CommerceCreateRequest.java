/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.migracion.model;

import pe.com.visanet.migracion.model.Processors;
import java.util.List;

/**
 *
 * @author Peter Galdamez
 */
public class CommerceCreateRequest {

    private String ruc;
    private String currency;
    private String mcc;
    private String tel;
    private String email;
    private String productId;
    private String byProductId;
    private String merchantId;
    private String merchantName;
    private String merchantGroupId;
    private String merchantGroupName;
    private String segment;
    private String phone;
    private boolean tokenizationEnabled;
    private boolean recurrencyEnabled;
    private boolean showFirstNameLastNameEnabled;
    private boolean installmentsEnabled;
    private boolean liquidationEnabled;
    private boolean dccEnabled;
    private boolean multibrandEnabled;
    private List<Processors> processors;
    private String mdd;
    private String pagoLinkName;
    
    
    
    
    
    private String cardNumber;
    private int binNumber;
    private String uuid;

    /**
     * @return the ruc
     */
    public String getRuc() {
        return ruc;
    }

    /**
     * @param ruc the ruc to set
     */
    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the mcc
     */
    public String getMcc() {
        return mcc;
    }

    /**
     * @param mcc the mcc to set
     */
    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    /**
     * @return the tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return the byProductId
     */
    public String getByProductId() {
        return byProductId;
    }

    /**
     * @param byProductId the byProductId to set
     */
    public void setByProductId(String byProductId) {
        this.byProductId = byProductId;
    }

    /**
     * @return the merchantId
     */
    public String getMerchantId() {
        return merchantId;
    }

    /**
     * @param merchantId the merchantId to set
     */
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the merchantGroupId
     */
    public String getMerchantGroupId() {
        return merchantGroupId;
    }

    /**
     * @param merchantGroupId the merchantGroupId to set
     */
    public void setMerchantGroupId(String merchantGroupId) {
        this.merchantGroupId = merchantGroupId;
    }

    /**
     * @return the merchantGroupName
     */
    public String getMerchantGroupName() {
        return merchantGroupName;
    }

    /**
     * @param merchantGroupName the merchantGroupName to set
     */
    public void setMerchantGroupName(String merchantGroupName) {
        this.merchantGroupName = merchantGroupName;
    }

    /**
     * @return the segment
     */
    public String getSegment() {
        return segment;
    }

    /**
     * @param segment the segment to set
     */
    public void setSegment(String segment) {
        this.segment = segment;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the tokenizationEnabled
     */
    public boolean isTokenizationEnabled() {
        return tokenizationEnabled;
    }

    /**
     * @param tokenizationEnabled the tokenizationEnabled to set
     */
    public void setTokenizationEnabled(boolean tokenizationEnabled) {
        this.tokenizationEnabled = tokenizationEnabled;
    }

    /**
     * @return the recurrencyEnabled
     */
    public boolean isRecurrencyEnabled() {
        return recurrencyEnabled;
    }

    /**
     * @param recurrencyEnabled the recurrencyEnabled to set
     */
    public void setRecurrencyEnabled(boolean recurrencyEnabled) {
        this.recurrencyEnabled = recurrencyEnabled;
    }

    /**
     * @return the showFirstNameLastNameEnabled
     */
    public boolean isShowFirstNameLastNameEnabled() {
        return showFirstNameLastNameEnabled;
    }

    /**
     * @param showFirstNameLastNameEnabled the showFirstNameLastNameEnabled to set
     */
    public void setShowFirstNameLastNameEnabled(boolean showFirstNameLastNameEnabled) {
        this.showFirstNameLastNameEnabled = showFirstNameLastNameEnabled;
    }

    /**
     * @return the installmentsEnabled
     */
    public boolean isInstallmentsEnabled() {
        return installmentsEnabled;
    }

    /**
     * @param installmentsEnabled the installmentsEnabled to set
     */
    public void setInstallmentsEnabled(boolean installmentsEnabled) {
        this.installmentsEnabled = installmentsEnabled;
    }

    /**
     * @return the liquidationEnabled
     */
    public boolean isLiquidationEnabled() {
        return liquidationEnabled;
    }

    /**
     * @param liquidationEnabled the liquidationEnabled to set
     */
    public void setLiquidationEnabled(boolean liquidationEnabled) {
        this.liquidationEnabled = liquidationEnabled;
    }

    /**
     * @return the dccEnabled
     */
    public boolean isDccEnabled() {
        return dccEnabled;
    }

    /**
     * @param dccEnabled the dccEnabled to set
     */
    public void setDccEnabled(boolean dccEnabled) {
        this.dccEnabled = dccEnabled;
    }

    /**
     * @return the multibrandEnabled
     */
    public boolean isMultibrandEnabled() {
        return multibrandEnabled;
    }

    /**
     * @param multibrandEnabled the multibrandEnabled to set
     */
    public void setMultibrandEnabled(boolean multibrandEnabled) {
        this.multibrandEnabled = multibrandEnabled;
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber the cardNumber to set
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the binNumber
     */
    public int getBinNumber() {
        return binNumber;
    }

    /**
     * @param binNumber the binNumber to set
     */
    public void setBinNumber(int binNumber) {
        this.binNumber = binNumber;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @param mode
     * @return the Mdd
     */
    public String getMdd(String mode) {

        String mdds = "{\"es\":[{\"name\":\"MDD1\",\"value\":\""+this.getMerchantId()+"\"},{\"name\":\"MDD2\",\"value\":\""+this.getMerchantName()+"\"},{\"name\":\"MDD10\",\"value\":\""+this.getSegment()+"\"},{\"name\":\"MDD11\",\"value\":\""+this.getMcc()+"\"}]}";
        
        if(!"".equals(mode)){
            mdds = String.format(mdds, ",{\"name\":\"MDD3\",\"value\":\""+mode+"\"}");
        }
        
        return mdds;
    }
    
    /**
     * @return the pagoLinkName
     */
    public String getPagoLinkName() {
        return this.getMerchantName().replace(" ", "");
    }

    /**
     * @param pagoLinkName the pagoLinkName to set
     */
    public void setPagoLinkName(String pagoLinkName) {
        this.pagoLinkName = pagoLinkName;
    }

    /**
     * @return the processors
     */
    public List<Processors> getProcessors() {
        return processors;
    }

    /**
     * @param processors the processors to set
     */
    public void setProcessors(List<Processors> processors) {
        this.processors = processors;
    }
    
    
}
