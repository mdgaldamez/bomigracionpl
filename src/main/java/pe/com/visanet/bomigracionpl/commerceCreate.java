/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.bomigracionpl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserResult;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AdminRespondToAuthChallengeRequest;
import com.amazonaws.services.cognitoidp.model.AdminRespondToAuthChallengeResult;
import com.amazonaws.services.cognitoidp.model.AttributeType;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.com.visanet.migracion.model.CommerceCreateRequest;
import pe.com.visanet.migracion.model.Processors;
import pe.com.visanet.migracion.model.ProductIdCodes;

/**
 *
 * @author Michael Galdámez
 */
public class commerceCreate {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(commerceCreate.class);

    private final String CREATE_COMMERCE_SQL;
    private final String CREATE_COMMERCE_MULTIBRAND_SQL;
    private final String VERIFY_COMMERCE_GROUP;
    private final String COGNITO_GROUP_POOL_ID;
    private final String AWS_ACCESS_KEY;
    private final String AWS_SECRET_KEY;
    private final String AWS_COMMERCE_GROUP_REGION;
    private final String AWS_COMMERCE_GROUP_CLIENT_ID;
    private final String CREATE_COMMERCE_GROUP_DB;
    private static DataSource dataSource;
    private static final String JNDI_NAME = "java:/backofficevn";
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    private static final String DB_URL = "jdbc:mysql://db1.prod.vnforapps.com:3306/backofficevn"; //db1.dev.quiputech.com:3306/backofficevn
    private static final String USER = "awsmaster";//ecore
    private static final String PASS = "VisaNet2018$$";//Av3$truz

    public commerceCreate() {
        this.AWS_COMMERCE_GROUP_CLIENT_ID=System.getProperty("AwsCommerceGroupClientId", "3pd3j8egkm3m9m4kifpv9v3i4n"); //Dev 10lv0617o5dic51ebsnqeiijb7
        this.AWS_COMMERCE_GROUP_REGION=System.getProperty("AwsCommerceGroupRegion", "us-east-1");
        this.AWS_SECRET_KEY = System.getProperty("AwsSecretKey", "wwhREopQswznJzd1VPtS/8W1solrQ6BTnEmDxNXa"); //oSpR/hEWyA0Xi0GPctk3pJ+JH/76DTiL4aQv+GmJ
        this.AWS_ACCESS_KEY = System.getProperty("AwsAccessKey", "AKIAJPLXPO2AGMPZD5DA"); //AKIAJLHKCP4INCUJ67EA
        this.COGNITO_GROUP_POOL_ID = System.getProperty("CognitoGroupPoolId", "us-east-1_pQHHu111b"); //Dev us-east-1_2cJ1Se1fI 
        this.VERIFY_COMMERCE_GROUP = "SELECT * FROM bo_commerce_group where group_code=?";
        this.CREATE_COMMERCE_GROUP_DB="INSERT INTO bo_commerce_group(name, user, password, email, state, group_code)"
                + " values(?,?,?,?,?,?)";
        this.CREATE_COMMERCE_MULTIBRAND_SQL = "INSERT INTO bo_multibrand (commerce_code, brand, commerce_brand_id, countable, `key`, active,processor)"+
                " values(?,?,?,?,?,?,?)";
        this.CREATE_COMMERCE_SQL = "INSERT INTO bo_commerce" 
                 + "(commerce_code, commerce_group, ruc, \n" +
                    "name, currency, mcc, tel, email, tokenizer, recurrent, \n" +
                    "show_names, show_quotas, state, automated_settle, \n" +
                    "fast_payment, link_type_id, \n" +
                    "sub_product, \n" +
                    "mdd, \n" +
                    "pagolinkname, \n" +
                    "is_cybersource_enabled, is_force_vbv, is_multibrand, \n" +
                    "is_qr_enabled, is_dcc_enabled," +
                    "product_id, subproduct_id, trx_flow) "
                + "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                            "?, ?, ?, ?, ?, ?, ?, ?)";
    }
    
        public void verify(CommerceCreateRequest body) throws NotFoundException {
        
        if (body.getRuc() == null || body.getRuc().length()< 11 || body.getRuc().length() > 11)
        {
            LOGGER.error("ruc es requerido y de 11 caracteres");
        }
        
        if (body.getMerchantId() == null || "".equals(body.getMerchantId()) || body.getMerchantId().length() !=9)
        {
            LOGGER.error("merchantId comercio es requerido y de 9 caracteres");
        }
        
        if (body.getMerchantName()== null || "".equals(body.getMerchantName()) || body.getMerchantName().length()> 100 )
        {
            LOGGER.error("merchantName de comercio es requerido y no debe sobrepasar los 100 caracteres");
        }
        
        if (body.getMerchantGroupId()== null || "".equals(body.getMerchantGroupId()) )
        {
            LOGGER.error("merchantGroupId de comercio es requerido");
        }
        if (body.getMerchantGroupName()== null || "".equals(body.getMerchantGroupName()) || body.getMerchantGroupName().length()> 50 )
        {
            LOGGER.error("merchantGroupName es requerido y/o un máximo de 50 caracteres");
        }
        
        if (body.getCurrency()== null || "".equals(body.getCurrency()) || body.getCurrency().length()> 3 || body.getCurrency().length() < 3 )
        {
            LOGGER.error("currency es requerido y de 3 caracteres");
        }
        
        if (body.getMcc()== null || "".equals(body.getMcc()) || body.getMcc().length()> 4 )
        {
            LOGGER.error("mcc es requerido y de 4 caracteres");
        }
        if (body.getSegment()== null || "".equals(body.getSegment()) || body.getSegment().length()> 3 )
        {
            LOGGER.error("Segment es requerido y de 3 caracteres");
        }
        if (body.getEmail()== null || "".equals(body.getEmail()) || body.getEmail().length()> 45 )
        {
            LOGGER.error("Email es requerido y un máximo de 45 caracteres");
        }
        if (body.getPhone()!= null && ("".equals(body.getPhone()) || body.getPhone().length()> 20) )
        {
            LOGGER.error("Phone debe contener un máximo de 20 caracteres");
        }
        if (body.isMultibrandEnabled() && (body.getProcessors() == null || body.getProcessors().isEmpty()))
        {
            LOGGER.error("processors es requerido");
        }
        if (body.isMultibrandEnabled() && (body.getProcessors() != null || !body.getProcessors().isEmpty()))
        {
            int i=0;
            String msg="processors[%s]";
            for(Processors processor : body.getProcessors())
            {
                if(processor.getProcessor()==null || "".equals(processor.getProcessor()))
                {
                    LOGGER.error(String.format(msg, i)+"-processor es requerido");
                }
                if(processor.getBrands()==null || processor.getBrands().isEmpty())
                {
                    LOGGER.error(String.format(msg, i)+"-brands es requerido");
                }
                if(processor.getCode()==null || "".equals(processor.getCode()))
                {
                    LOGGER.error(String.format(msg, i)+"-code es requerido");
                }
                if(processor.getKey()==null || "".equals(processor.getKey()))
                {
                    LOGGER.error(String.format(msg, i)+"-key es requerido");
                }
                i++;
            }
        }
        
        int codigo = Integer.valueOf(body.getProductId());//ProductIdCodes.getProductCode(body.getProductId());
        int subcodigo = Integer.valueOf(body.getByProductId());//ProductIdCodes.getSubProductCode(body.getByProductId());
        if (codigo == -1 || subcodigo == -1)
        {
            LOGGER.error("productId o byProductId no registrados");
        }
        
        String mddmode = "";
        if(codigo == ProductIdCodes.CE )
        {
            if(subcodigo == ProductIdCodes.TUVITRINA)
                mddmode="TV";
            if(subcodigo == ProductIdCodes.PAGOLINK)
                mddmode="LINK";
            if(subcodigo == ProductIdCodes.PAGOAPP)
                mddmode = "APP";
            if(subcodigo == ProductIdCodes.PAGOWEB)
                mddmode = "WEB";
        }
        if(codigo == ProductIdCodes.TELEPAGO)
            mddmode ="MOTO";
        
        try (Connection con = getConnection()) {
            try {
                con.setAutoCommit(false);
                int commerceGroupId = verifyOrCreateCommerceGroup(body,con);
                
                PreparedStatement stmt = con.prepareStatement(CREATE_COMMERCE_SQL);
                stmt.setString(1, body.getMerchantId());
                stmt.setInt(2, commerceGroupId);
                stmt.setString(3, body.getRuc());
                stmt.setString(4, body.getMerchantName());
                stmt.setString(5, body.getCurrency());
                stmt.setString(6, body.getMcc());
                stmt.setString(7, body.getPhone());
                stmt.setString(8, body.getEmail());
                stmt.setInt(9, body.isTokenizationEnabled()?1:0);
                stmt.setInt(10, body.isRecurrencyEnabled()?1:0);
                stmt.setInt(11, body.isShowFirstNameLastNameEnabled()?1:0);
                stmt.setInt(12, body.isInstallmentsEnabled()?1:0);
                stmt.setString(13, "Activo");
                stmt.setInt(14, body.isLiquidationEnabled()?1:0);
                stmt.setInt(15, 0);
                stmt.setString(16, "URL");
                stmt.setInt(17, subcodigo);
                stmt.setString(18, body.getMdd(mddmode));
                stmt.setString(19, body.getPagoLinkName());
                stmt.setInt(20, 1);
                stmt.setInt(21, 0);
                stmt.setInt(22, body.isMultibrandEnabled()?1:0);
                stmt.setInt(23, 0);
                stmt.setInt(24, body.isDccEnabled()?1:0);
                stmt.setInt(25, codigo);
                stmt.setInt(26, subcodigo);
                stmt.setString(27, "3");

                if (body.isMultibrandEnabled()) {
                    PreparedStatement stmtBrand = con.prepareStatement(CREATE_COMMERCE_MULTIBRAND_SQL);
                    //commerce_code, brand, commerce_brand_id, countable, key, active,processor
                    for(Processors multiConfig : body.getProcessors())
                    {
                        stmtBrand.setString(1, body.getMerchantId());
                        stmtBrand.setString(2, String.join(";", multiConfig.getBrands()));
                        stmtBrand.setString(3, multiConfig.getCode());
                        stmtBrand.setInt(4, multiConfig.isCountable()?1:0);
                        stmtBrand.setString(5, multiConfig.getKey());
                        stmtBrand.setInt(6, multiConfig.isEnabled()?1:0);
                        stmtBrand.setString(7, multiConfig.getProcessor());
                        
                        int res = stmtBrand.executeUpdate();
                    }
                    
                    //insert config
                    //String multi = "INSERT INTO bo_multibran(commerce_code,brand,commerce_brand_id,countable,key,active,processor) values(?,?,?,?,?,?,?)";
                    //stmt.setString(0, "");
                    //  stmt.setInt(2, 0);
                }

                int uprs = stmt.executeUpdate();
                

                con.commit();

            } catch (SQLException ex) {
                con.rollback();
                LOGGER.error(ex.getLocalizedMessage(), ex);
                LOGGER.error(ex.getMessage());
            } finally {
                try {
                    if(!con.isClosed()) {
                        con.close();
                    }
                } catch(Exception e) {
                    LOGGER.error(e.getLocalizedMessage(), e);
                }
            }

        } catch (Exception ex) {
            
            LOGGER.error(ex.getLocalizedMessage(), ex);
            LOGGER.error(ex.getMessage());
        }
    }

    private int verifyOrCreateCommerceGroup(CommerceCreateRequest body, Connection con) throws SQLException {
        PreparedStatement stmtVerifyGroup = con.prepareStatement(VERIFY_COMMERCE_GROUP);
        stmtVerifyGroup.setString(1, body.getMerchantGroupId());                
        ResultSet uprs = stmtVerifyGroup.executeQuery();
        boolean exists = uprs.first();
        if(!exists)
        {
            AdminCreateUserRequest createUserOdCommerceGroupOnCognito = createUserOdCommerceGroupOnCognito(body);
            //name, user, password, email, state, group_code
            PreparedStatement stmtCreateGroup = con.prepareStatement(CREATE_COMMERCE_GROUP_DB);
            stmtCreateGroup.setString(1, "Grupo "+ body.getMerchantGroupName());
            stmtCreateGroup.setString(2, body.getEmail());
            stmtCreateGroup.setString(3, createUserOdCommerceGroupOnCognito.getTemporaryPassword());
            stmtCreateGroup.setString(4, body.getEmail());
            stmtCreateGroup.setString(5, "Activo");
            stmtCreateGroup.setString(6, body.getMerchantGroupId());
            
            int executeUpdate = stmtCreateGroup.executeUpdate();
            uprs = stmtVerifyGroup.executeQuery();
            exists = uprs.first();
            return uprs.getInt("id");
        }
        else{
            return uprs.getInt("id");
        }        
    }    
    
    private AdminCreateUserRequest createUserOdCommerceGroupOnCognito(CommerceCreateRequest body) 
    {
        AdminCreateUserRequest createUserRequest = new AdminCreateUserRequest();
        createUserRequest.setUsername(body.getEmail());
        String tmpPWD = randomPassword();
        createUserRequest.setTemporaryPassword(tmpPWD);
        
        createUserRequest.setUserPoolId(COGNITO_GROUP_POOL_ID);
        createUserRequest.setDesiredDeliveryMediums(Arrays.asList("EMAIL"));
        
        Collection<AttributeType> userAttributes = new ArrayList<>();
        AttributeType nameAtt = new AttributeType();
        nameAtt.setName("name");
        nameAtt.setValue(body.getMerchantGroupName());
        userAttributes.add(nameAtt);
        nameAtt = new AttributeType();
        nameAtt.setName("email");
        nameAtt.setValue(body.getEmail());
        userAttributes.add(nameAtt);
        nameAtt = new AttributeType();
        nameAtt.setName("given_name");
        nameAtt.setValue(body.getRuc());
        userAttributes.add(nameAtt);
        nameAtt = new AttributeType();
        nameAtt.setName("family_name");
        nameAtt.setValue(body.getMerchantId());
        userAttributes.add(nameAtt);
        createUserRequest.setUserAttributes(userAttributes);
        
        AWSCognitoIdentityProviderClientBuilder awsCognitClient = AWSCognitoIdentityProviderClientBuilder.standard();
        
        awsCognitClient.setRegion(AWS_COMMERCE_GROUP_REGION);
        AWSCredentials credentials =  new BasicAWSCredentials(AWS_ACCESS_KEY, AWS_SECRET_KEY);
          
        AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
        awsCognitClient.setCredentials(credentialsProvider);
        AWSCognitoIdentityProvider client = awsCognitClient.build();
        
        AdminCreateUserResult adminCreateUser = client.adminCreateUser(createUserRequest);
        
        AdminInitiateAuthRequest userAuth = new AdminInitiateAuthRequest();
        userAuth.setAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH);
        userAuth.setClientId(AWS_COMMERCE_GROUP_CLIENT_ID);
        userAuth.setUserPoolId(COGNITO_GROUP_POOL_ID);
        
        userAuth.addAuthParametersEntry("USERNAME", body.getEmail());
        userAuth.addAuthParametersEntry("PASSWORD", tmpPWD);
        
        AdminInitiateAuthResult adminInitiateAuth = client.adminInitiateAuth(userAuth);
        
        Map<String, String> challengeParameters = adminInitiateAuth.getChallengeParameters();
        
        challengeParameters.put("USERNAME", body.getEmail());
        challengeParameters.put("PASSWORD", tmpPWD);
        challengeParameters.put("NEW_PASSWORD", tmpPWD);
        
        AdminRespondToAuthChallengeRequest adminRespondToAuthChallengeRequest = new AdminRespondToAuthChallengeRequest();
        
        adminRespondToAuthChallengeRequest.setChallengeName(adminInitiateAuth.getChallengeName());
        adminRespondToAuthChallengeRequest.setClientId(AWS_COMMERCE_GROUP_CLIENT_ID);
        adminRespondToAuthChallengeRequest.setSession(adminInitiateAuth.getSession());
        adminRespondToAuthChallengeRequest.setUserPoolId(COGNITO_GROUP_POOL_ID);
        adminRespondToAuthChallengeRequest.setChallengeResponses(challengeParameters);
        
        AdminRespondToAuthChallengeResult adminRespondToAuthChallenge = client.adminRespondToAuthChallenge(adminRespondToAuthChallengeRequest);

        return createUserRequest;
    }
    
    private String randomPassword() {
        String[] arr = {"a","H","xk","9y","pL","2","3","5","8","10","85","?",".","-","z","MM","UD","p0","008","$!","!."};
        StringBuilder stringBuilder= new StringBuilder();
        for(int i=0;i<10;i++)
        {
            stringBuilder.append(arr[(int)(Math.random() * 20)]);
        }
        return stringBuilder.toString();
        
    }    

    public Connection getConnection2() {
        try {
            if (dataSource == null) {
                InitialContext ic = new InitialContext();
                dataSource = (DataSource) ic.lookup(JNDI_NAME);
            }
            return dataSource.getConnection();
        } catch (NamingException | SQLException e) {
            LOGGER.error(e.getLocalizedMessage());
            return null;
        }
    }
    
    protected Connection getConnection() {
        try {
            Connection conn = null;
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            return conn;
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return null;
        }
    }
}
