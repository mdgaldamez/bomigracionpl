/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.migracion.model;

/**
 *
 * @author Michael Galdámez
 */
public class ComercioGrupo {
    
    private String name;
    private String user;
    private String email;
    private String state;
    private String group_code;
    private String accessKeyId;
    
    public ComercioGrupo(String accessKeyId, String name, String user, String email, String state, String group_code) {
        setAccessKeyId(accessKeyId);
        setName(name);
        setUser(user);
        setEmail(email);
        setState(state);
        setGroup_code(group_code);
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }
    
}
