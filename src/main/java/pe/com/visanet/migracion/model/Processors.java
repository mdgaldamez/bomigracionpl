/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.migracion.model;

import java.util.List;

/**
 *
 * @author Denitos
 */
public class Processors {

    
    private String processor;
    private List<String> brands;
    private String code;
    private String key;
    private boolean enabled;
    private boolean countable;
    
 
    
    /**
     * @return the processor
     */
    public String getProcessor() {
        return processor;
    }

    /**
     * @param procesor the processor to set
     */
    public void setProcessor(String procesor) {
        this.processor = procesor;
    }

    /**
     * @return the brands
     */
    public List<String> getBrands() {
        return brands;
    }

    /**
     * @param brands the brands to set
     */
    public void setBrands(List<String> brands) {
        this.brands = brands;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the countable
     */
    public boolean isCountable() {
        return countable;
    }

    /**
     * @param countable the countable to set
     */
    public void setCountable(boolean countable) {
        this.countable = countable;
    }

    /**
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key the key to set
     */
    public void setKey(String key) {
        this.key = key;
    }
}
