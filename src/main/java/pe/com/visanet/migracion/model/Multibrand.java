/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.migracion.model;

/**
 *
 * @author Michael Galdámez
 */
public class Multibrand {
    
    private String commerce_code;
    private String brand;
    private String commerce_brand_id;
    private String countable;
    private String key;
    private String active;
    private String createdOn;        
    private String processor;
    
    public Multibrand(String commerce_code, String brand, String commerce_brand_id, String countable, String key, String active, String createdOn, String processor) {
        setCommerce_code(commerce_code);
        setBrand(brand);
        setCommerce_brand_id(commerce_brand_id);
        setCountable(countable);
        setKey(key);
        setActive(active);
        setCreatedOn(createdOn);
        setProcessor(processor);
    }

    public String getCommerce_code() {
        return commerce_code;
    }

    public void setCommerce_code(String commerce_code) {
        this.commerce_code = commerce_code;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCommerce_brand_id() {
        return commerce_brand_id;
    }

    public void setCommerce_brand_id(String commerce_brand_id) {
        this.commerce_brand_id = commerce_brand_id;
    }

    public String getCountable() {
        return countable;
    }

    public void setCountable(String countable) {
        this.countable = countable;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }
}
