package pe.com.visanet.migracion.model;

/**
 *
 * @author Michael Galdámez
 */
public class Comercio {
    
    //private String id;
    private String commerce_group;
    private String ruc;
    private String name;
    private String currency;
    private String mcc;
    private String tel;
    private String email;
    private String tokenizer;
    private String recurrent;
    private String show_names;
    private String show_quotas;
    private String state;
    private String automated_settle;
    private String link_type_id;
    //private String commerce_logo;
    private String commerce_code;
    private String verified_by_visa;
    private String rule_engine;
    private String sub_product;
    private String mdd;
    //private String special_fields;
    private String trx_flow;
    //private String creationDate;
    //private String expiration_minutes;
    private String pagolinkname;
    private String id_comercioGrupo;
    private String name_comercioGrupo;
    private String user;
    //private String password;
    private String email_comercioGrupo;
    private String state_comercioGrupo;
    private String group_code;
    private String cybersource;
    private String product_id;
    private String subproduct_id;
    
    public Comercio( String commerce_group, String ruc, String name, String currency, String mcc, String tel,String email, String tokenizer, String recurrent, 
            String show_names, String show_quotas, String state, String automated_settle, String link_type_id, String commerce_code, String rule_engine, 
            String sub_product, String mdd, String trx_flow, String id_comercioGrupo, String name_comercioGrupo, String user, String email_comercioGrupo, 
            String state_comercioGrupo, String group_code, String pagolinkname, String cybersource, String verified_by_visa, String product_id, String subproduct_id) {
            setCommerce_group(commerce_group);
            setRuc(ruc);
            setName(name);
            setCurrency(currency);
            setMcc(mcc);
            setTel(tel);
            setEmail(email);
            setTokenizer(tokenizer);
            setRecurrent(recurrent);
            setShow_names(show_names);
            setShow_quotas(show_quotas);
            setState(state);
            setAutomated_settle(automated_settle);
            setLink_type_id(link_type_id);
            setCommerce_code(commerce_code);
            setRule_engine(rule_engine);
            setSub_product(sub_product);
            setMdd(mdd);
            setTrx_flow(trx_flow);
            setId_comercioGrupo(id_comercioGrupo);
            setName_comercioGrupo(name_comercioGrupo);
            setUser(user);
            setEmail_comercioGrupo(email_comercioGrupo);
            setState_comercioGrupo(state_comercioGrupo);
            setGroup_code(group_code);
            setPagolinkname(pagolinkname);
            setCybersource(cybersource);
            setVerified_by_visa(verified_by_visa);
            setProduct_id(product_id);
            setSubproduct_id(subproduct_id);
            
    }

    public String getPagolinkname() {
        return pagolinkname;
    }

    public void setPagolinkname(String pagolinkname) {
        this.pagolinkname = pagolinkname;
    }
    
    public String getVerified_by_visa() {
        return verified_by_visa;
    }

    public void setVerified_by_visa(String verified_by_visa) {
        this.verified_by_visa = verified_by_visa;
    }
    
    public String getCybersource() {
        return cybersource;
    }

    public void setCybersource(String cybersource) {
        this.cybersource = cybersource;
    }

    public String getCommerce_group() {
        return commerce_group;
    }

    public void setCommerce_group(String commerce_group) {
        this.commerce_group = commerce_group;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTokenizer() {
        return tokenizer;
    }

    public void setTokenizer(String tokenizer) {
        this.tokenizer = tokenizer;
    }

    public String getRecurrent() {
        return recurrent;
    }

    public void setRecurrent(String recurrent) {
        this.recurrent = recurrent;
    }

    public String getShow_names() {
        return show_names;
    }

    public void setShow_names(String show_names) {
        this.show_names = show_names;
    }

    public String getShow_quotas() {
        return show_quotas;
    }

    public void setShow_quotas(String show_quotas) {
        this.show_quotas = show_quotas;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAutomated_settle() {
        return automated_settle;
    }

    public void setAutomated_settle(String automated_settle) {
        this.automated_settle = automated_settle;
    }

    public String getLink_type_id() {
        return link_type_id;
    }

    public void setLink_type_id(String link_type_id) {
        this.link_type_id = link_type_id;
    }

    public String getCommerce_code() {
        return commerce_code;
    }

    public void setCommerce_code(String commerce_code) {
        this.commerce_code = commerce_code;
    }

    public String getRule_engine() {
        return rule_engine;
    }

    public void setRule_engine(String rule_engine) {
        this.rule_engine = rule_engine;
    }

    public String getSub_product() {
        return sub_product;
    }

    public void setSub_product(String sub_product) {
        this.sub_product = sub_product;
    }

    public String getMdd() {
        return mdd;
    }

    public void setMdd(String mdd) {
        this.mdd = mdd;
    }

    public String getTrx_flow() {
        return trx_flow;
    }

    public void setTrx_flow(String trx_flow) {
        this.trx_flow = trx_flow;
    }

    public String getId_comercioGrupo() {
        return id_comercioGrupo;
    }

    public void setId_comercioGrupo(String id_comercioGrupo) {
        this.id_comercioGrupo = id_comercioGrupo;
    }

    public String getName_comercioGrupo() {
        return name_comercioGrupo;
    }

    public void setName_comercioGrupo(String name_comercioGrupo) {
        this.name_comercioGrupo = name_comercioGrupo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail_comercioGrupo() {
        return email_comercioGrupo;
    }

    public void setEmail_comercioGrupo(String email_comercioGrupo) {
        this.email_comercioGrupo = email_comercioGrupo;
    }

    public String getState_comercioGrupo() {
        return state_comercioGrupo;
    }

    public void setState_comercioGrupo(String state_comercioGrupo) {
        this.state_comercioGrupo = state_comercioGrupo;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }
    
    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSubproduct_id() {
        return subproduct_id;
    }

    public void setSubproduct_id(String subproduct_id) {
        this.subproduct_id = subproduct_id;
    }
}
