package pe.com.visanet.bomigracionpl;

import pe.com.visanet.migracion.model.Comercio;
import pe.com.visanet.migracion.model.Multibrand;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael Galdamez
 */
public class DatabaseManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseManager.class);
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    private static final String DB_URL = "jdbc:mysql://db1.prod.vnforapps.com:3306/ecommerce"; //db1.dev.quiputech.com:3306/backofficevn
    private static final String USER = "awsmaster";//ecore
    private static final String PASS = "VisaNet2018$$";//Av3$truz

    protected Connection getConnection() {
        try {
            Connection conn = null;
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            return conn;
        } catch (SQLException | ClassNotFoundException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return null;
        }
    }
    
    public long insertGroupComercios(
            String accessKeyId,
            String name,
            String user,
            String email,
            String state,
            String group_code) throws ParseException {

        String queryString = "INSERT INTO backofficevn.bo_commerce_group_copy (access_key_id, name, user, email, state, group_code) "
                + "VALUES (?, ?, ?, ?, ?, ?)";

        LOGGER.trace("Query: " + queryString);
        int result = 0;
        Connection conx = getConnection();

        try {
            PreparedStatement stmt = conx.prepareStatement(queryString);
            stmt.setObject(1, accessKeyId);
            stmt.setObject(2, name);
            stmt.setObject(3, user);
            stmt.setObject(4, email);
            stmt.setObject(5, state);
            stmt.setObject(6, group_code);
            result = stmt.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = 0;
        } finally {
            try {
                if (!conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
        return result;
    }

    public long insertComercios(Comercio comercio) throws ParseException {

        String queryString = "INSERT INTO backofficevn.bo_commerce ( pagolinkname, commerce_group, ruc, name, currency, mcc, tel, email, tokenizer, recurrent, show_names, "
               + "show_quotas, state, automated_settle, link_type_id, commerce_code, rule_engine, sub_product, mdd, trx_flow, is_cybersource_enabled, verified_by_visa, product_id, "
               + "subproduct_id ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? ) "; 
        LOGGER.trace("Query: " + queryString);
        
        int result = 0;
        Connection conx = getConnection();

        try {
            PreparedStatement stmt = conx.prepareStatement(queryString);
            int i = 1;
                stmt.setString(i++, comercio.getPagolinkname());
                stmt.setString(i++, comercio.getCommerce_group());
                stmt.setString(i++, comercio.getRuc());
                stmt.setString(i++, comercio.getName());          
                stmt.setString(i++, comercio.getCurrency());
                stmt.setString(i++, comercio.getMcc());
                stmt.setString(i++, comercio.getTel());
                stmt.setString(i++, comercio.getEmail());
                stmt.setString(i++, comercio.getTokenizer());
                stmt.setString(i++, comercio.getRecurrent());
                stmt.setString(i++, comercio.getShow_names());
                stmt.setString(i++, comercio.getShow_quotas());
                stmt.setString(i++, comercio.getState());
                stmt.setString(i++, comercio.getAutomated_settle());
                stmt.setString(i++, comercio.getLink_type_id());
                stmt.setString(i++, comercio.getCommerce_code());
                stmt.setString(i++, comercio.getRule_engine());
                stmt.setString(i++, comercio.getSub_product());
                stmt.setString(i++, comercio.getMdd());
                stmt.setString(i++, comercio.getTrx_flow());
                stmt.setString(i++, comercio.getCybersource());
                stmt.setString(i++, comercio.getVerified_by_visa());
                stmt.setString(i++, comercio.getProduct_id());
                stmt.setString(i++, comercio.getSubproduct_id());
            result = stmt.executeUpdate();

        } catch (Exception e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = 0;
        } finally {
            try {
                if (!conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
        return result;
    }
    
    public long updateComercios(
        String commerce_code,
        String mdd) throws ParseException {

        String queryString = "UPDATE `backofficevn`.`bo_commerce` SET `mdd`= ? WHERE `commerce_code`= ? ";

        LOGGER.trace("Query: " + queryString);
        
        int result = 0;
        Connection conx = getConnection();

        try {
            PreparedStatement stmt = conx.prepareStatement(queryString);
            stmt.setObject(1, mdd);
            stmt.setObject(2, commerce_code);        
            result = stmt.executeUpdate();
            
        } catch (SQLException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = 0;
        } catch (Exception e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = 0;
        } finally {
            try {
                if (!conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
        return result;
    }
    
    public long insertBOPLink(
            String commerceId,
            String linkTypeId) throws ParseException {

        String queryString = "INSERT INTO backofficevn.bo_plink (amount, terms, link_type, commerce_id, visual_count, expiration_date,"
                + "design, size, state, count, type, link_name, max_trx, contentType, creation_date) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, null, sysdate())";

        LOGGER.trace("Query: " + queryString);
        int result = 0;
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, 100);
        String expirationDate = DATE_FORMAT.format(c.getTime());
        Connection conx = getConnection();

        try {
            PreparedStatement stmt = conx.prepareStatement(queryString);
            stmt.setObject(1, "0");
            stmt.setObject(2, "0");
            stmt.setObject(3, "0");
            stmt.setObject(4, commerceId);
            stmt.setObject(5, "0");
            stmt.setObject(6, expirationDate);
            stmt.setObject(7, linkTypeId);
            stmt.setObject(8, "SMLL");
            stmt.setObject(9, "Activo");
            stmt.setObject(10, "0");
            stmt.setObject(11, "UNQ");
            stmt.setObject(12, "pagolink");
            stmt.setObject(13, "0");
            result = stmt.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = 0;
        } finally {
            try {
                if (!conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
        return result;
    }
        
    public String queryBOCommerce(
            String merchantId,  
            String... fields) throws ParseException {
        
        String result = "";
        Connection conx = getConnection();
        try {

            String queryString = "SELECT * FROM backofficevn.bo_commerce WHERE ";
            queryString += !"".equals(merchantId) ? "commerce_code = ? AND   " : "";
            queryString = queryString.substring(0, queryString.length() - 6);

            LOGGER.trace("Query: " + queryString);
            PreparedStatement query = conx.prepareStatement(queryString);
            int i = 1;
            if (!"".equals(merchantId)) {
                query.setString(i++, merchantId);
            }

            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                result = rs.getString("name");
            }
            return result;
            
        } catch (SQLException e) {
            System.out.println("Error: " + e.getMessage());
            return result;
        } finally {
            try {
                if (!conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
    }
    
    public long insertMultibrand(Multibrand m) throws ParseException {

        String queryString = "INSERT INTO `backofficevn`.`bo_multibrand` (`commerce_code`, `brand`, `commerce_brand_id`, `countable`, `key`, `active`, `createdOn`, `processor`) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)"; 
        LOGGER.trace("Query: " + queryString);
        
        int result = 0;
        Connection conx = getConnection();

        try {
            PreparedStatement stmt = conx.prepareStatement(queryString);
            int i = 1;
            if (!"".equals(m.getCommerce_code())) {
                stmt.setString(i++, m.getCommerce_code());
            }
            if (!"".equals(m.getBrand())) {
                stmt.setString(i++, m.getBrand());
            }
            if (!"".equals(m.getCommerce_brand_id())) {
                stmt.setString(i++, m.getCommerce_brand_id());
            }
            if (!"".equals(m.getCountable())) {
                stmt.setString(i++, m.getCountable());
            }
            if (!"".equals(m.getKey())) {
                stmt.setString(i++, m.getKey());
            }
            if (!"".equals(m.getActive())) {
                stmt.setString(i++, m.getActive());
            }
            if (!"".equals(m.getCreatedOn())) {
                stmt.setString(i++, m.getCreatedOn());
            }
            if (!"".equals(m.getProcessor())) {
                stmt.setString(i++, m.getProcessor());
            }
            result = stmt.executeUpdate();

        } catch (Exception e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = 0;
        } finally {
            try {
                if (!conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
        return result;
    }
}
