package pe.com.visanet.migracion.model;

/**
 *
 * @author Michael Galdamez
 */
public final class User {
 
    private String name;
    private String given_name;
    private String family_name;
    private String email;
    private String state;
    private String entity;

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }
  
    public String getName() {
        if (name == null)
        name = "";
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }

    public String getGiven_name() {
        if (given_name == null)
        given_name = "";
        return given_name;
    }
 
    public void setGiven_name(String given_name) {
        this.given_name = given_name;
    }
    
    public String getFamily_name() {
        if (family_name == null)
        family_name = "";
        return family_name;
    }
 
    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }
    
    public String getEmail() {
        if (email == null)
        email = "";
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getState() {
        if (state == null)
        state = "";
        return state;
    }
 
    public void setState(String state) {
        this.state = state;
    }
}
