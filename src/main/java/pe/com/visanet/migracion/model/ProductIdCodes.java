/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.visanet.migracion.model;

/**
 *
 * @author Denitos
 */
public class ProductIdCodes {

    public static int CE = 1;
    public static int REC = 2;
    public static int MPOS = 3;
    public static int TELEPAGO = 4;
    
    public static int PAGOLINK = 1;
    public static int PAGOWEB = 2;
    public static int PASARELA  = 3;
    public static int PAGOAPP = 4;
    public static int TUVITRINA = 5;
    public static int RECREC = 6;
    public static int PSTREC = 7;
    public static int FPREC = 8;
    public static int PLUGIN = 9;
    public static int URL = 10;
    public static int CF = 11;

    public static int getSubProductCode(String code) {
        switch (code) {
            case "TUVITRINA":
                return ProductIdCodes.TUVITRINA;
            case "PAGOLINK":
                return ProductIdCodes.PAGOLINK;
            case "PAGOAPP":
                return ProductIdCodes.PAGOAPP;
            case "PAGOWEB":
                return ProductIdCodes.PAGOWEB;
            case "PSTREC":
                return ProductIdCodes.PSTREC;
            case "FPREC":
                return ProductIdCodes.FPREC;
            case "CF":
                return ProductIdCodes.CF;
            case "URL":
                return ProductIdCodes.URL;
            case "PLUGIN":
                return ProductIdCodes.PLUGIN;
            case "REC":
                return ProductIdCodes.RECREC;
            case "PASARELA":
                return ProductIdCodes.PASARELA;
            default:
                return -1;
        }
    }

    public static int getProductCode(String code) {
        switch (code) {
            case "CE":
                return ProductIdCodes.CE;
            case "REC":
                return ProductIdCodes.REC;
            case "MPOS":
                return ProductIdCodes.MPOS;
            case "TELEPAGO":
                return ProductIdCodes.TELEPAGO;
            default:
                return -1;
        }
    }
    
    public static String getProductName(int code) {
        switch (code) {
            case 1:
                return "CE";
            case 2:
                return "REC";
            case 3:
                return "MPOS";
            case 4:
                return "TELEPAGO";
            default:
                return "";
        }
    }
    
    public static String getSubProductName(int code) {
        switch (code) {
            case 1:
                return "PAGOLINK";
            case 2:
                return "PAGOWEB";
            case 3:
                return "PASARELA";
            case 4:
                return "PAGOAPP";
            case 5:
                return "TUVITRINA";
            case 6:
                return "RECREC";
            case 7:
                return "PSTREC";
            case 8:
                return "FPREC";
            case 9:
                return "PLUGIN";
            case 10:
                return "URL"; 
            case 11:
                return "CF"; 
            default:
                return "";
        }
    }
}
