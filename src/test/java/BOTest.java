
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.*;
import org.testng.annotations.Test;
import pe.com.visanet.bomigracionpl.DatabaseManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Michael Galdámez
 */
public class BOTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BOTest.class);
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    
    public BOTest() {
        BasicConfigurator.configure();
        LogManager.getRootLogger().setLevel(Level.TRACE);
    }
    
    @Test(enabled = false)
    public void test() throws ParseException {
        DatabaseManager db = new DatabaseManager();
        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, 100);
        String timeStamp = DATE_FORMAT.format(c.getTime());
        System.out.print("Fecha actual: " + timeStamp);
        
        String comer = "982022502, 982022502, 982022502";
        String nombreComercio = "";
        String customentity = "[ ";
        String[] tokens = comer.split(",");
        for(String t : tokens) {
            nombreComercio = db.queryBOCommerce(t.trim());
            customentity += "{\"id\":\"" + t + "\",\"name\":\"" + t + " - " + nombreComercio + "\"},";
        }
        customentity = customentity.substring(0, customentity.length() - 1);
        customentity += " ]";
        System.out.println(customentity);
    }
    
}
