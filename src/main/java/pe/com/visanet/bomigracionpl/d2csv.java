package pe.com.visanet.bomigracionpl;

import pe.com.visanet.migracion.model.Comercio;
import pe.com.visanet.migracion.model.Multibrand;
import pe.com.visanet.migracion.model.Usuario1;
import pe.com.visanet.migracion.model.Usuario;
import org.json.JSONException;
import org.json.JSONObject;
import com.csvreader.CsvReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import pe.com.visanet.migracion.model.CommerceCreateRequest;

public class d2csv
{
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(d2csv.class);

    public d2csv(JSONObject config)
    {
        if (validateConfig(config))
        {
            try
            {
                DatabaseManager db = new DatabaseManager();
                String file = config.getString("path");
                if (config.has("action")){
                    switch (String.valueOf(config.get("action"))) {
                        case "InsertarComercios":
                            InsertarComercios(file, db);
                            break;
                        case "InsertarUsuarios":
                            InsertarUsuarios(file, db);
                            break;
                        case "InsertarMultibrand":
                            InsertarMultibrand(file,db);
                            break;
                        case "UpdateUsuarios":
                            UpdateGroupUser(file);
                            break;
                        case "UpdateComercios":
                            UpdateComercios(file);
                            break;
                        case "CrearUsuariosCognito":
                            createUsers(file, db);
                            break;
                        case "RecrearUsuariosCognito":
                            recreateUsers(file, db);
                            break;
                        case "commerceCreate":
                            commerceCreate(file);
                            break;
                    }
                }
            }
            catch (JSONException ex)
            {
                System.out.println(ex.getMessage());
            } catch (ParseException ex) {
                Logger.getLogger(d2csv.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private Boolean validateConfig(JSONObject config){
        Boolean valid = true;
        if (!config.has("path"))
        {
            System.out.println("config parameter 'path' is missing.");
            valid = false;
        }
        /*if (!config.has("tableName"))
        {
            System.out.println("config parameter 'tableName' is missing.");
            valid = false;
        }*/
        return valid;
    }
    
    public static void InsertarComercios(String file, DatabaseManager db) throws ParseException {
        try { 
            List<Comercio> comercios = new ArrayList<>();

            CsvReader comercios_import = new CsvReader(file);
            comercios_import.readHeaders();

            while (comercios_import.readRecord()) {
                String commerce_group = comercios_import.get("Codigo_Grupo");
                String ruc = comercios_import.get("RUC");
                String name = comercios_import.get("Nombre");
                String currency = comercios_import.get("Moneda");
                if(currency.equals("DOLARES")){
                    currency = "USD";
                }else if(currency.equals("SOLES")){
                    currency = "PEN";
                }
                String mcc = comercios_import.get("MCC");
                String tel = comercios_import.get("Telefono");
                String email = comercios_import.get("Correo");
                String tokenizer = comercios_import.get("Habilitar_Tokenizacion");
                String recurrent = comercios_import.get("Habilitar_Recurrencia");
                String show_names = comercios_import.get("Mostrar_FirstName_LastName");
                String show_quotas = comercios_import.get("Mostrar_Cuotas");
                String state = comercios_import.get("Estado_Comercio");
                String automated_settle = comercios_import.get("Liquidacion_Automatica");
                String link_type_id = comercios_import.get("Disenho_Link");
                if(link_type_id.equals("Link URL")){
                    link_type_id = "URL";
                }
                String commerce_logo = comercios_import.get("Logo_Comercio");
                String commerce_code = comercios_import.get("Codigo_Comercio");
                //String verified_by_visa = comercios_import.get("");
                String rule_engine = comercios_import.get("Rule_Engine");
                String sub_product = comercios_import.get("Sub-Producto");
                if(sub_product.equals("Pago Link")){
                    sub_product = "PLNK";
                }else if(sub_product.equals("Tu Vitrina")){
                    sub_product = "TVRN";
                }else if(sub_product.equals("Pago Web")){
                    sub_product = "PWB";
                }
                String mdd1 = comercios_import.get("MDD1");
                String mdd2 = comercios_import.get("MDD2");
                String mdd3 = comercios_import.get("MDD3");
                String mdd10 = comercios_import.get("MDD10");
                String mdd11 = comercios_import.get("MDD11");
                String mdd = "{\"es\":[{\"name\":\"MDD1\",\"value\":\"" + mdd1 +"\"},"
                                    + "{\"name\":\"MDD2\",\"value\":\"" + mdd2 +"\"},"
                                    + "{\"name\":\"MDD3\",\"value\":\"" + mdd3 +"\"},"
                                    + "{\"name\":\"MDD10\",\"value\":\"" + mdd10 +"\"},"
                                    + "{\"name\":\"MDD11\",\"value\":\"" + mdd11 +"\"}]}";
                //String special_fields = comercios_import.get("");
                String trx_flow = comercios_import.get("Flujo_Transaccional");
                if(trx_flow.equals("Antifraude + Verified by Visa + Autorizacion")){
                    trx_flow = "AF+VbV+AUT";
                }
                String pagolinkname = comercios_import.get("Nombre_Link_Pago");
                String is_cybersource_enabled = !comercios_import.get("Cybersource").isEmpty() ? comercios_import.get("Cybersource") : "0";
                String verified_by_visa = comercios_import.get("Verified_By_Visa");
                String product_id = comercios_import.get("product_id");
                String subproduct_id = comercios_import.get("subproduct_id");
                String id = comercios_import.get("Id_Grupo");
                String nameGroup = "";//comercios_import.get("Nombre_Grupo");
                String user = "";//comercios_import.get("Nombre_Usuario");
                String password = "";//comercios_import.get("Password");
                String emailUser = "";//comercios_import.get("E-mail_Usuario");
                String stateGroup = "";//comercios_import.get("Estado_Grupo");
                String group_code = "";//comercios_import.get("Codigo_Grupo");

                comercios.add(new Comercio(commerce_group, ruc, name, currency, mcc, tel, email, tokenizer, recurrent,show_names, show_quotas, state, automated_settle, 
                        link_type_id, commerce_code, rule_engine, sub_product, mdd, trx_flow,id, nameGroup, user, emailUser, stateGroup, group_code, pagolinkname, 
                        is_cybersource_enabled, verified_by_visa, product_id, subproduct_id));

            }

            comercios_import.close();
            System.out.println("Procesar " + comercios.size());
            int n = 0;
            for (Comercio c : comercios) {

                long created = db.insertComercios(c);
                if (created <= 0) {
                    System.out.println(c.getName() + " commerce not created");
                } else {
                    System.out.println(c.getName() + " created");
                }
                if(n%50==0){
                    System.out.println("Insertando: " + String.valueOf(n));
                }
                n++;
            }
            
        } catch (Exception e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } 
    }
    
    public static void InsertarUsuarios(String file, DatabaseManager db) throws ParseException{
        try {
            CsvReader usuarios_import = new CsvReader(file);
            List<Usuario> usuarios = new ArrayList<>();

            usuarios_import.readHeaders();

            while (usuarios_import.readRecord()) {
                String nombre = usuarios_import.get("Nombre");
                String apellido = usuarios_import.get("Apellido");
                String perfil = usuarios_import.get("Perfil");
                String email = usuarios_import.get("Email");
                String comercio = usuarios_import.get("Codigo_Comercio");
                String ruc = usuarios_import.get("RUC");

                usuarios.add(new Usuario(nombre, apellido, perfil, email, comercio, ruc));
            }

            usuarios_import.close();
            System.out.println("Procesar " + usuarios.size());
            int n = 0;
            for (Usuario us : usuarios) {
                
                String comer = us.getCodigoComercio();
                comer = comer.replace("'","");
                String nombreComercio = "";
                String customentity = "[ ";
                String[] tokens = comer.split(",");
                for(String t : tokens) {                  
                    nombreComercio = db.queryBOCommerce(t.trim());
                    if (nombreComercio.equals("")) {
                        System.out.println(t.trim() + " commerce not found");
                    }
                    customentity += "{\"id\":\"" + t + "\",\"name\":\"" + t + " - " + nombreComercio + "\"}, ";
                }
                customentity = customentity.substring(0, customentity.length() - 2);
                customentity += " ]";
                
                boolean created = CognitoManager.createCognitoAccount(us.getEmail(), us.getNombre(), us.getApellido(), customentity, us.getRuc());

                if (!created) {
                    System.out.println(us.getEmail().toLowerCase() + " Account not created");
                } else {
                    System.out.println(us.getEmail().toLowerCase() + " created");
                    //
                    if (!us.getPerfil().isEmpty()) {
                        String perfilReal = "";
                        if(us.getPerfil().equals("Administrador")){
                            perfilReal = "ComercioAdminTelepago"; //PerfilCome
                        }
                        CognitoManager.addUserToGroup(us.getEmail().toLowerCase(), perfilReal);
                    }
                }
                if(n%50==0){
                    System.out.println("Insertando: " + String.valueOf(n));
                }
                n++;
            }

        } catch (Exception e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } 
    }
    
    public static void InsertarMultibrand(String file, DatabaseManager db) throws ParseException{
        try {
            CsvReader multibrand = new CsvReader(file);
            List<Multibrand> records = new ArrayList<>();

            multibrand.readHeaders();

            while (multibrand.readRecord()) {
                String commerce_code = multibrand.get("commerce_code");
                String brand = multibrand.get("brand");
                String commerce_brand_id = multibrand.get("commerce_brand_id");
                String countable = multibrand.get("countable");
                String key = multibrand.get("key");
                String active = multibrand.get("active");
                String createdOn = multibrand.get("createdOn");
                String processor = multibrand.get("processor");

                records.add(new Multibrand(commerce_code, brand, commerce_brand_id, countable, key, active, createdOn, processor));
            }

            multibrand.close();
            System.out.println("Procesar " + records.size());
            int n = 0;
            for (Multibrand m : records) {
                
                long created = db.insertMultibrand(m);

                if (created < 1) {
                    System.out.println(m.getCommerce_code() + " Commerce not created");
                } else {
                    System.out.println(m.getCommerce_code() + " Commerce created");
                }
                if(n%50==0){
                    System.out.println("Insertando: " + String.valueOf(n));
                }
                n++;
            }

        } catch (Exception e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } 
    }
    
    public static void UpdateGroupUser(String file) throws ParseException{
        try {
            CsvReader usuarios_import = new CsvReader(file);
            List<Usuario> usuarios = new ArrayList<>();

            usuarios_import.readHeaders();

            while (usuarios_import.readRecord()) {
                String email = usuarios_import.get("Usuario");
                String comercio = usuarios_import.get("Comercio");
                String perfil = usuarios_import.get("Perfil");

                usuarios.add(new Usuario(email, comercio, perfil, "", "", ""));
            }

            usuarios_import.close();
            String group = "ComercioAdmin"; //

            for (Usuario us : usuarios) {
        
                boolean updated = CognitoManager.addUserToGroup(us.getEmail(), group);

                if (!updated) {
                    System.out.println(us.getEmail() + " Account not updated");
                } else {
                    System.out.println(us.getEmail() + " updated");
                }
            }

        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }
    
    public static void UpdateComercios(String file) throws ParseException {
        try {

            DatabaseManager db = new DatabaseManager();

            CsvReader comercios_import = new CsvReader(file);
            comercios_import.readHeaders();

            while (comercios_import.readRecord()) {
                String commerce_code = comercios_import.get("Codigo_Comercio");
                String mdd1 = comercios_import.get("MDD1");
                String mdd2 = comercios_import.get("MDD2");
                String mdd3 = comercios_import.get("MDD3");
                String mdd10 = comercios_import.get("MDD10");
                String mdd11 = comercios_import.get("MDD11");
                String mdd = "{\"es\":[{\"name\":\"MDD1\",\"value\":\"" + mdd1 +"\"},"
                                    + "{\"name\":\"MDD2\",\"value\":\"" + mdd2 +"\"},"
                                    + "{\"name\":\"MDD3\",\"value\":\"" + mdd3 +"\"},"
                                    + "{\"name\":\"MDD10\",\"value\":\"" + mdd10 +"\"},"
                                    + "{\"name\":\"MDD11\",\"value\":\"" + mdd11 +"\"}]}";
                
               long updated = db.updateComercios(commerce_code, mdd);
                if (updated <= 0) {
                    System.out.println(commerce_code + " commerce not updated");
                } else {
                    System.out.println(commerce_code + " updated");
                }           
            }
            comercios_import.close();
        } catch (FileNotFoundException e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);   
        }
    }
    
    public static void createUsers(String file, DatabaseManager db) throws ParseException{
        try {
            CsvReader usuarios_import = new CsvReader(file);
            List<Usuario1> usuarios = new ArrayList<>();

            usuarios_import.readHeaders();

            while (usuarios_import.readRecord()) {
                String username = usuarios_import.get("Username");
                String nombre = usuarios_import.get("Nombre");
                String apellido = usuarios_import.get("Apellido");
                String perfil = usuarios_import.get("Perfil");
                String email = usuarios_import.get("Email");
                String idInstitucion = usuarios_import.get("IdInstitucion");
                String tipoDocumento = usuarios_import.get("TipoDocumento");
                String genero = usuarios_import.get("Genero");
                String numeroDocumento = usuarios_import.get("NroDocumento");
                String grupo = usuarios_import.get("Grupo");
                String familyName = usuarios_import.get("FamilyName");
                String preferredUsername = usuarios_import.get("PreferredUsername");
                String name = usuarios_import.get("Name");
                String givenName = usuarios_import.get("GivenName");
                
                usuarios.add(new Usuario1(username, grupo, genero, idInstitucion, tipoDocumento, numeroDocumento, nombre, apellido, email, perfil,
                                          familyName, preferredUsername, name, givenName));
            }

            usuarios_import.close();
            System.out.println("Procesar " + usuarios.size());
            int n = 0;
            for (Usuario1 us : usuarios) {
                
                boolean created = CognitoManager.createCognitoAccountNew(us);

                if (!created) {
                    System.out.println(us.getUsername().toLowerCase() + " Account not created");
                } else {
                    System.out.println(us.getUsername().toLowerCase() + " created");
                    //
                    if (!us.getGrupo().isEmpty()) {
                        CognitoManager.addUserToGroup(us.getUsername(), us.getGrupo());
                    }
                }
                if(n%50==0){
                    System.out.println("Insertando: " + String.valueOf(n));
                }
                n++;
            }

        } catch (Exception e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } 
    }
    
    public static void recreateUsers(String file, DatabaseManager db) throws ParseException{
        try {
            CsvReader usuarios_import = new CsvReader(file);
            List<Usuario1> usuarios = new ArrayList<>();

            usuarios_import.readHeaders();

            while (usuarios_import.readRecord()) {
                String username = usuarios_import.get("Username");
                String nombre = usuarios_import.get("Nombre");
                String apellido = usuarios_import.get("Apellido");
                String perfil = usuarios_import.get("Perfil");
                String email = usuarios_import.get("Email");
                String idInstitucion = usuarios_import.get("IdInstitucion");
                String tipoDocumento = usuarios_import.get("TipoDocumento");
                String genero = usuarios_import.get("Genero");
                String numeroDocumento = usuarios_import.get("NroDocumento");
                String grupo = usuarios_import.get("Grupo");
                String familyName = usuarios_import.get("FamilyName");
                String preferredUsername = usuarios_import.get("PreferredUsername");
                String name = usuarios_import.get("Name");
                String givenName = usuarios_import.get("GivenName");
                
                usuarios.add(new Usuario1(username, grupo, genero, idInstitucion, tipoDocumento, numeroDocumento, nombre, apellido, email, perfil,
                                          familyName, preferredUsername, name, givenName));
            }

            usuarios_import.close();
            System.out.println("Procesar " + usuarios.size());
            int n = 0;
            for (Usuario1 us : usuarios) {
                CognitoManager.deleteCognitoAccount(us.getUsername(), us.getPerfil());
                boolean created = CognitoManager.createCognitoAccountNew(us);

                if (!created) {
                    System.out.println(us.getUsername().toLowerCase() + " Account not created");
                } else {
                    System.out.println(us.getUsername().toLowerCase() + " created");
                    //
                    if (!us.getGrupo().isEmpty()) {
                        CognitoManager.addUserToGroup(us.getUsername(), us.getGrupo());
                    }
                }
                if(n%50==0){
                    System.out.println("Insertando: " + String.valueOf(n));
                }
                n++;
            }

        } catch (Exception e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        } 
    }
    
    public static void commerceCreate(String file){
        
        try { 
            List<Comercio> comercios = new ArrayList<>();

            CsvReader comercios_import = new CsvReader(file);
            comercios_import.readHeaders();

            CommerceCreateRequest CCRequest = new CommerceCreateRequest();
            
            while (comercios_import.readRecord()) {
                
                CCRequest.setRuc(comercios_import.get("ruc"));
                CCRequest.setCurrency(comercios_import.get("currency"));
                CCRequest.setMcc(comercios_import.get("mcc"));
                CCRequest.setEmail(comercios_import.get("email"));
                CCRequest.setProductId(comercios_import.get("productId"));
                CCRequest.setByProductId(comercios_import.get("byProductId"));
                CCRequest.setMerchantId(comercios_import.get("merchantId"));
                CCRequest.setMerchantName(comercios_import.get("merchantName"));
                CCRequest.setMerchantGroupId(comercios_import.get("merchantGroupId"));
                CCRequest.setMerchantGroupName(comercios_import.get("merchantGroupName"));
                CCRequest.setSegment(comercios_import.get("segment"));
                CCRequest.setPhone(comercios_import.get("phone"));
                CCRequest.setTokenizationEnabled(Boolean.parseBoolean(comercios_import.get("tokenizationEnabled")));
                CCRequest.setRecurrencyEnabled(Boolean.parseBoolean(comercios_import.get("recurrencyEnabled")));
                CCRequest.setShowFirstNameLastNameEnabled(Boolean.parseBoolean(comercios_import.get("showFirstNameLastNameEnabled")));
                CCRequest.setInstallmentsEnabled(Boolean.parseBoolean(comercios_import.get("installmentsEnabled")));
                CCRequest.setLiquidationEnabled(Boolean.parseBoolean(comercios_import.get("liquidationEnabled")));
                CCRequest.setDccEnabled(Boolean.parseBoolean(comercios_import.get("dccEnabled")));
                CCRequest.setMultibrandEnabled(Boolean.parseBoolean(comercios_import.get("multibrandEnabled")));
                CCRequest.setProcessors(null);
                String mdd1 = comercios_import.get("MDD1");
                String mdd2 = comercios_import.get("MDD2");
                String mdd3 = comercios_import.get("MDD3");
                String mdd10 = comercios_import.get("MDD10");
                String mdd11 = comercios_import.get("MDD11");
                String mdd = "{\"es\":[{\"name\":\"MDD1\",\"value\":\"" + mdd1 +"\"},"
                                    + "{\"name\":\"MDD2\",\"value\":\"" + mdd2 +"\"},"
                                    + "{\"name\":\"MDD3\",\"value\":\"" + mdd3 +"\"},"
                                    + "{\"name\":\"MDD10\",\"value\":\"" + mdd10 +"\"},"
                                    + "{\"name\":\"MDD11\",\"value\":\"" + mdd11 +"\"}]}";
                //CCRequest.setMdd(mdd);
                commerceCreate CC = new commerceCreate();
                CC.verify(CCRequest);
            }
            comercios_import.close();
            LOGGER.info("Procesar " + comercios.size());
        } catch (Exception e ) {
            LOGGER.error(e.getLocalizedMessage(), e);
        }        
    }
}

